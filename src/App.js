import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'; 
import '../node_modules/font-awesome/css/font-awesome.min.css';
import {Component} from 'react';
import SignUp from './components/signup';
import Login from './components/login';
import Home from './components/home';
import Forgot from './components/forgot';
import Product from './components/product';
import Footer from './components/footer';
import Pay from './components/pay';
import Banner from './components/banner';
import ShopCart from './components/shopcart';
import DetailProduct from './components/detailproduct';
import HomeWatch from './components/homewatch';
import {BrowserRouter,Route, Link,Switch, Router} from 'react-router-dom';


class App extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         Email: "",
    //         Password: "",
    //         Address: "",
    //     };
    //     this.onHandleChange = this.onHandleChange.bind(this);
    //     this.onHandleSubmit = this.onHandleSubmit.bind(this);
        
    // }

    // onHandleChange(event) {
    //     var target = event.target;
    //     var name = target.name;
    //     var value = target.value;
    //     this.setState({
    //         [name]: value,
    //     });
    // }

    // onHandleSubmit(event) {
    //     alert("Welcom to MyAppStore  " + JSON.stringify(this.state));//
    //     event.preventDefault();
    //     console.log(this.state);
    // }
    
    render(){
        return(
            <BrowserRouter>
           
                  <Switch> 
                  <Route path="/login" component={Login}/>
                    <Route path="/signup" component={SignUp}/>
                    <Route path="/home" component={Home}/>
                    <Route path="/forgot" component={Forgot}/>
                    <Route path="/product" component={Product}/>
                    <Route path="/footer" component={Footer}/>
                    <Route path="/shopcart" component={ShopCart}/>
                    <Route path="/pay" component={Pay}/>
                    <Route path="/banner" component={Banner}/>
                    <Route path="/detailproduct" component={DetailProduct}/>
                    <Route path="/homewatch" component={HomeWatch}/>
                    <Route path="/" component={Home}/>  
                  </Switch>
                  
            </BrowserRouter>
        )}
}
export default App;
