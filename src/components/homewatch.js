import React, {Component, Components} from 'react';
import {Link} from 'react-router-dom';
import {Button} from 'reactstrap';
import Slider from "react-slick";


class HomeWatch extends Component{
    
        render(){
        
            const settings = {
              dots: true,
              infinite: true,
              speed: 500,
              slidesToShow: 5,
              slidesToScroll: 1,
              className:"slides"
            };
        return(
            <div className="root">
            <div className="header">
            <div className="header-form">
               <h2 className="custom-h2">MyAppStore- World Phone</h2>
        <div className="col-md-12 navbar-cps_box-main">
                        <li className="box-man"><a href="#">Trang chủ MyAppStore</a>
                     <li>  <a href=""><img src="apple.jpg"alt="" width="100px" height="100px" /> </a> </li> 
                            <li><a href="#">Điện thoại  <span className="arrow">
                            {/* <select multiple className="lua-chon">
                                <option value="0">Chọn sản phẩm điện thoại bạn cần </option>
                                <option value="34">Iphone </option>
                                <option value="46">Samsung </option>
                                <option value="68">Oppo </option>
                                <option value="812">Vivo</option>
                                </select> */}
                                </span></a></li>
                            <li>
                                <input type="search" name="search" placeholder="Tìm kiếm sản phẩm..." />
                                <span class="icon"><i class="fa fa-search"></i></span></li>
                                {/* <ul className="form-menu">
                                <li><a href="#" >Iphone</a></li>
                                <li><a href="#" >Samsung</a></li>
                                <li><a href="#" >Oppo</a></li>
                                <li><a href="#" >Vivo</a></li>
                                </ul> */}
                              
                                <li><span class="icon"><i class="fa fa-phone"></i></span><a href="#">Liên hệ</a></li> 
                                <li><span class="icon"><i class="fa fa-shopping-cart"></i></span><a href="#">Giỏ hàng</a></li>
                                <li><span class="icon"><i class="fa fa-cogs"></i></span><a href="#">Chăm sóc khách hàng</a> </li>
                                <li><span class="icon"><i class="fa fa-question-circle"></i></span><a href="#">Tư vấn</a> </li>
                                </li>
            </div>
    </div>
            </div>
            <div className="container-main">
                <div className="banner">
                <div className="row">
                                    <div className="wrapper-container">
                                        <div className="wrapper-banner">
                                        {/* <div className="slides.slick-next" tabindex="0" role="button" aria-label="Next slide">
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>  */}
                                        {/* <div className="slides.slick-prev" tabindex="0" role="button" aria-label="Previous slide">
                                          <i class="fa fa-chevron-left" aria-hidden="true"></i> 
                                             */}
                                            <div className="banner-img">
                                                <a href="#">
                                                <div className="slides.slick-prev" tabindex="0" role="button" aria-label="Previous slide">
                                             <i class="fa fa-chevron-left" aria-hidden="true"></i> 
                <Slider {...settings}>
           <div>
             <h3> <div className="custom-bannerimg">
                                 <img src="b.png"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-bannerimg">
                                 <img src="c.png"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-bannerimg">
                                 <img src="d.png"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-bannerimg">
                                 <img src="e.png"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-bannerimg">
                                 <img src="f.png"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-bannerimg">
                                 <img src="g.jpg"alt=""/>
                                 </div></h3>
           </div>
         </Slider>
         <div className="slides.slick-next" tabindex="0" role="button" aria-label="Next slide">
                                             <i class="fa fa-chevron-right" aria-hidden="true"></i> 
                                             </div>
                                             </div>
        </a>
      
                                </div>
                               
                            </div>
                            </div>
                        </div>
                        {/* <div className="col-md-12 mb-4">
                            <a href="#">
                                <img className="banner-imgright"  alt="Asus Zenbook"src="29.jpg"></img>
                            </a>
                        </div> */}
                    </div>
                    <h1 className="gia-tri">Mua điện thoại giá hợp lí</h1>
                        <div className="left-main">
                        <div className="row">
                                <div className="col-sm-2">
                                    <div className="des">Hãng Sản Xuất </div>
                                    <a href="#">Apple chính hãng</a><br></br>
                                     <a href="#">SamSung</a><br></br>
                                     <a href="#">Xiaomi</a><br></br>
                                     <a href="#">ViVo</a><br></br>
                                     <a href="#">Amazfit</a><br></br>
                                     <a href="#">Garmin</a><br></br>
                                     <a href="#">UAG<br></br>
                                     <a href="#">Huawie</a><br></br>
                                     <a href="#">Oppo</a><br></br>
                                    {/* <div className="des">Bộ nhớ </div>
                                         <a href="#">16GB</a><br></br>
                                         <a href="#">16GB - 32GB</a><br></br>
                                         <a href="#">32GB - 64GB</a><br></br>
                                         <a href="#">64GB - 128GB</a><br></br>
                                         <a href="#">128GB - 256GB</a><br></br>
                                         <a href="#">256GB - 512GB</a><br></br> */}
                                    <div className="des">Kích cỡ mặt đồng hồ </div>
                                     <a href="#">Cho nam (42-47mm)</a><br></br>
                                     <a href="#">Cho nữ( dưới 40mm)</a><br></br>
                                    {/* <div className="des">
                                        Kiểu màn hình </div>
                                         <a href="#">Giọt nước</a><br></br>
                                         <a href="#">Tai thỏ</a><br></br>
                                         <a href="#">Tràn viền</a><br></br>
                                         <a href="#">Màn hình gập</a><br></br> */}
                                    <div className="des">
                                       Chất liệu dây</div>
                                     <a href="#">Cao su</a><br></br>
                                     <a href="#">Da</a><br></br>
                                     <a href="#">kim loại</a><br></br>
                                     <a href="#">Vải</a><br></br>
                                     <a href="#">Silicon</a><br></br>
                                    <div className="des">Chất liệu case</div>
                                         <a href="#">Nhôm</a><br></br>
                                         <a href="#">Thép</a><br></br>
                                    <div className="des">Tiện ích tính năng </div>
                                     <a href="#">Hỗ trợ eSim</a><br></br>
                                     <a href="#">Man hình luôn bật</a><br></br>
                                     <a href="#">Nghe gọi trên đồng hồ </a><br></br>
                                     <a href="#">Nghe nhạc trên đồng hồ</a><br></br>
                                     {/* <a href="#">Định vị GPS</a><br></br>
                                     <a href="#">Kính cường lực</a><br></br>
                                     <a href="#">Sạc dự phòng</a><br></br>
                                     <a href="#">Đế để điện thoại cho xem youtube, chương trình...</a><br></br> */}
                                      <div className="des">Các tính năng tiện ích khác </div>
                                     <a href="#">Đo huyết áp</a><br></br>
                                     <a href="#">Hỗ trợ Wifi</a><br></br>
                                     <a href="#">Chụp ảnh trên đồng hồ </a><br></br>
                                     <a href="#">Đo lượng oxy trong máu</a><br></br>
                                </a>
                                </div>
                                <div className="col-sm-10">
                                <div className="container-products">
                                <ul class="cols cols-5">
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                                <img src="82.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							<span className="text-giam-gia">-10%</span>
						</div>
					 		<p class="sticker-hotsale-2"></p>
													
                                        </div>

                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Đồng hồ Xiaomi Mi Watch</p>

                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								5.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">3.290.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            7 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                        {/* <p class="gift-cont">Tặng tai nghe bluetooth QCY T7</p> */}
                        </div>		
                                        </div> 
                                     </li>   
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                                <img src="81.jpg"> 
                                                </img>
                                            </a>
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Đồng hồ Huami Amazfit GTS 2e	</p>

                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <span className="price">3.490.000&nbsp;₫</span>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            3 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                        <p class="gift-cont">Tặng tai nghe bluetooth QCY T7</p></div>		
                                        </div>
                                    </li>
                                    <li>
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                            <span className="text-giam-gia">-17%</span>
                                                <img src="83.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							{/* <span className="text-giam-gia">-17%</span> */}
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Samsung Galaxy Watch Active 2	</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								5.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">3.290.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            7 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                        {/* <p class="gift-cont">Tặng tai nghe bluetooth QCY T7</p> */}
                        </div>		
                                        </div>
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                            <span className="text-giam-gia">-12%</span>
                                                <img src="84.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							{/* <span className="text-giam-gia">-12%</span> */}
						</div>
					 		<p class="sticker-hotsale-2"></p>
													
                                        </div>

                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Đồng hồ Huami Amazfit Bip U Pro</p>

                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								2.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">1.290.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            5 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                        {/* <p class="gift-cont">Tặng tai nghe bluetooth QCY T7</p> */}
                        </div>		
                                        </div> 
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                                <img src="85.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							<span className="text-giam-gia">-12%</span>
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Apple Watch SE | Apple VN/A	</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								12.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">10.290.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            7 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                         <p class="gift-cont">Giảm thêm 300.000 đ khi mua kèm Airpod  </p> 
                        </div>		
                                        </div> 
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                            <span className="text-giam-gia">-19%</span>
                                                <img src="87.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							{/* <span className="text-giam-gia">-19%</span> */}
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Đồng hồ Huami Amazfit GTS	</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								4.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">2.290.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            10 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                         {/* <p class="gift-cont">Giảm thêm 300.000 đ khi mua kèm Airpod  </p>  */}
                        </div>		
                                        </div> 
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                                <img src="88.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							<span className="text-giam-gia">-12%</span>
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Apple Watch Series 3 | Apple VN/A	</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								4.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">6.590.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            5 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                         <p class="gift-cont">Giảm thêm 300.000 đ khi mua kèm Airpod  </p> 
                        </div>		
                                        </div> 
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                            <span className="text-giam-gia">-12%</span>
                                                <img src="89.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							{/* <span className="text-giam-gia">-12%</span> */}
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Oppo Watch</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								5.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">6.990.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            9 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                         {/* <p class="gift-cont">Giảm thêm 300.000 đ khi mua kèm Airpod  </p>  */}
                        </div>		
                                        </div> 
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                                <img src="90.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							<span className="text-giam-gia">-15%</span>
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Đồng hồ Huawie Watch</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								3.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">2.290.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            1 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                         {/* <p class="gift-cont">Giảm thêm 300.000 đ khi mua kèm Airpod  </p>  */}
                        </div>		
                                        </div> 
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                                <img src="92.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							<span className="text-giam-gia">-22%</span>
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Apple Watch Series 6 | Apple VN/A	</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								15.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">11.990.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            15 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                         <p class="gift-cont">Giảm thêm 300.000 đ khi mua kèm Airpod  </p> 
                        </div>		
                                        </div> 
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
                                                <img src="94.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							<span className="text-giam-gia">-15%</span>
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">Apple Watch Series 6 | Apple VN/A	</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								22.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">20.290.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            15 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                         <p class="gift-cont">Giảm thêm 300.000 đ khi mua kèm Airpod  </p> 
                        </div>		
                                        </div> 
                                    </li>
                                    <li>
                                    <div className="lt-product-group-image">
                                            <a href="#">
							<span className="text-giam-gia">-13%</span>

                                                <img src="96.jpg"> 
                                                </img>
                                            </a>
                                            <div class="sticker-giam-gia">
							{/* <span className="text-giam-gia">-13%</span> */}
						</div>
					 		<p class="sticker-hotsale-2"></p>
														
                                        </div>
                                        <div className="lt-product-group-info">
                                            <a href="#">
                                            <p className="custom-h3pro">SamSung Galaxy Active 2 4G LTE</p>
                                            </a>
                                            <div className="price-box">
                                            <span className="regular-price">
                                            <div class="price-box">
													<p class="old-price">
								11.990.000&nbsp;₫
							</p>
							<p class="special-price">
								<span class="price">6.290.000&nbsp;₫</span>
							</p>
											</div>
                                            </span>
                                            </div>
                                            <div className="clear"></div>
											<span class="ratingresult">
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            <i class="fa fa-star checked"></i>
                                            3 đánh giá
                                            </span>
                                            <div className="clear"></div>
					<div className="promotion">
                         {/* <p class="gift-cont">Giảm thêm 300.000 đ khi mua kèm Airpod  </p>  */}
                        </div>		
                                        </div> 
                                    </li>
                                    </ul>
                                    </div>
                                    <h2>Đồ chơi công nghệ- Làm cuộc sống tiện nghi hơn</h2>
                                    <p>
                                    Công nghệ ngày càng phát triển vượt bậc, tạo ra những thiết bị hiện đại, thực hiện được nhiều việc mà trước kia con người chỉ có thể làm thủ công bằng tay, qua đó làm cuộc sống của chúng ta trở nên tiện nghi hơn, thoải mái hơn.</p>

<p>Tại CellphoneS, đồ chơi công nghệ được bán với mức giá phải chăng đi kèm nhiều dịch vụ mang lại lợi ích cao cho người dùng. Nếu bạn đang tìm nơi mua đồ chơi công nghệ giá rẻ uy tín, hệ thống cửa hàng My AppStore chắc chắn là địa chỉ mà bạn không thể bỏ qua.</p>
<p>Tham khảo thêm: Đồng hồ Xiaomi Mi Band 6 chính hãng, sắp được lên kệ trên toàn quốc.
                                    </p>
                                    <h2>Tại sao nên mua đồ chơi công nghệ thông minh</h2>
                                    <p><ul>
                                    Mẫu mã và chức năng phong phú, phù hợp với nhiều nhu cầu trong sinh hoạt, làm việc lẫn giải trí.</ul></p>
                                    <p><ul>
                                    Đa dạng mức giá, dành cho mọi đối tượng người dùng ở nhiều tầng lớp khác nhau.</ul></p>
                                    <p><ul>
                                    Hỗ trợ con người ở nhiều hoạt động, làm cuộc sống trở nên dễ dàng và tiện lợi hơn.</ul></p>
                             </div>
                        </div>
                </div>
              </div>
              
            
            <div className="footer">
            <div classNameName="footer-form">
            <div classNameName="col-md-12 footer__box-contact bg-white">
                <div className="container-footer">
                <div className="row py-4">
                <div className="col-md-3 col-sm-3 col-xs-3">
                <div className="row">
                <ul className="list-unstyled m-0">
                 <li className="mb-3"> 
                <p className="font-16 mb-2"><label>Địa chỉ cửa hàng</label></p>
                <p className="mb-0"><a className="test-text" href="#">Tìm cửa hàng gần nhất</a></p>
                <p className="mb-0"><a className="test-text" href="#">Mua hàng Online</a></p>
                <div className="box-cttt">
                <p className="font-16 mb-0"><label>Phương thức thanh toán</label></p>
                <ul className="col-md-10 col-sm-10  col-sm-10 d-flex flex-wrap m-0 list-unstyled">
                {/* <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-visa"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mastercard"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-atm"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-alepay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-zalopay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-vnpay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-momo"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mpos"></em></a></li> */}
                <span class="icon"><i class="fa fa-cc-visa"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-mastercard"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-paypal"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-jcb"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-credit-card-alt"></i></span><a href="#"></a>
                </ul>
                </div>
                 </li> 
                </ul>
                </div>
              
                </div>
                <div className="col-md-3 col-sm-3 col-xs-3" >
                    <div className="row">
                    <ul className="list-unstyled m-0">
                    <p className="font-16 mb-0"><label>Dịch vụ hỗ trợ tư vấn khách hàng</label></p>
                    {/* <li className="mb-4"> */}
                    {/* <p className="mb-0">Liên hệ mua hàng: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 22h00)</p><br></br>
                    <p className="mb-0">Liên hệ bảo hành: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 21h00)</p> */}
                    <p>Liên hệ mua hàng- Hotline :1800.2020<br></br>Thời gian làm việc: 24/24</p>
                    <p>Liên hệ bảo hành- Hotline :1800.2020 <br></br>Thời gian làm việc :8h AM - 12h PM</p>
                    {/* </li> */}
                    <li>
                   </li>
                   </ul>
                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Mua hàng và thanh toán Online</p>
                        <p>Mua hàng và thanh toán trả góp qua Online</p>
                        <p>Tra cứu thông tin đơn hàng</p>
                        <p>Tra cứu thông tin bảo hành sản phẩm</p>
                        <p>Trung tâm bảo hành chính hãng</p>
                        <p>Dịch vụ bảo hành và sưa chữa điện thoại</p>

                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Quy chế hoạt động</p>
                        <p>Chính sách bảo hành</p>
                        <p>Điều kiện chính sách</p>
                        <p>Thông tin pháp lý</p>
                        <p>Giới thiệu cửa hàng</p>
                        <p>Giấy phép kinh doanh</p>

                    </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
            </div>
        )
 }
 }
 export default HomeWatch;

