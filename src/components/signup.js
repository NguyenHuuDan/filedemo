import {Component} from 'react';
import {Button, Form, FormGroup, Label, Input} from 'reactstrap';
import {Link} from 'react-router-dom';
 class SignUp extends Component{
    constructor(props) {
      super(props);
      this.state = {
          Email: "",
          Password: "",
          Confirm:"",
          Address: "",
      };
      this.onHandleChange = this.onHandleChange.bind(this);
      this.onHandleSubmit = this.onHandleSubmit.bind(this);
      // this.onhandleClick = this.onhandleClick.bind(this);
  }
  

    onHandleChange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value,
        });
    }

    onHandleSubmit(event) {
        alert("Welcom to MyAppStore  "+ JSON.stringify(this.state.Email)+JSON.stringify(this.state.Password)+ JSON.stringify(this.state.Confirm)+ JSON.stringify(this.state.Address));
        event.preventDefault();
        console.log(this.state);
       
    }
    validationForm() {
        const re = /\S+@\S+\.\S+/;
        if (re.test(this.state.Email)&& re.test(this.state.Password) ) return false;
        return true; 
      }
    render() {
        return (
            <div onSubmit={this.onHandleSubmit}>
            <Form className="signup-form">
                <h1 className="custom-h1">Welcom to MyAppStore</h1>
                <h2 className="text-center">SignUp MyAppStore</h2>
                <FormGroup>
                    <Label>Email</Label>
                    <Input type="Email" name="Email" placeholder="Email" onChange={this.onHandleChange}
                           value={this.state.Email}/>
                </FormGroup>
                <FormGroup>
                    <Label>Mật khẩu</Label>
                    <Input type="Password" name="Password" placeholder="Password" onChange={this.onHandleChange}
                           value={this.state.Password}/>
                </FormGroup>
                <FormGroup>
                    <Label>Xác nhận mật khẩu</Label>
                    <Input type="Confirm" name="Confirm" placeholder="Confirm" onChange={this.onHandleChange}
                           value={this.state.Confirm}/>
                </FormGroup>
                <FormGroup>
                    <Label>Địa chỉ</Label>
                    <Input type="Address" name="Address" placeholder="Address" onChange={this.onHandleChange}
                           value={this.state.Address}/>
                </FormGroup>
                <Button className="btn-lg btn-dark btn-block">Đăng Kí
                <Link to="/login" />
                </Button>


            </Form>
</div>
        )

    }
}
export default SignUp;