import React, {Component, Components} from 'react';
import {Link} from 'react-router-dom';
import {Button} from 'reactstrap';


class DetailProduct extends Component{
    render(){
        return(
            <div className="root">
            <div className="header">
            <div className="header-form">
               <h2 className="custom-h2">MyAppStore- World Phone</h2>
        <div className="col-md-12 navbar-cps_box-main">
                        <li className="box-man"><a href="#">Trang chủ MyAppStore</a>
                     <li>  <a href=""><img src="apple.jpg"alt="" width="100px" height="100px" /> </a> </li> 
                            <li><a href="#">Điện thoại  <span className="arrow">
                            {/* <select multiple className="lua-chon">
                                <option value="0">Chọn sản phẩm điện thoại bạn cần </option>
                                <option value="34">Iphone </option>
                                <option value="46">Samsung </option>
                                <option value="68">Oppo </option>
                                <option value="812">Vivo</option>
                                </select> */}
                                </span></a></li>
                            <li>
                                <input type="search" name="search" placeholder="Tìm kiếm sản phẩm..." />
                                <span class="icon"><i class="fa fa-search"></i></span></li>
                                {/* <ul className="form-menu">
                                <li><a href="#" >Iphone</a></li>
                                <li><a href="#" >Samsung</a></li>
                                <li><a href="#" >Oppo</a></li>
                                <li><a href="#" >Vivo</a></li>
                                </ul> */}
                              
                                <li><span class="icon"><i class="fa fa-phone"></i></span><a href="#">Liên hệ</a></li> 
                                <li><span class="icon"><i class="fa fa-shopping-cart"></i></span><a href="#">Giỏ hàng</a></li>
                                <li><span class="icon"><i class="fa fa-cogs"></i></span><a href="#">Chăm sóc khách hàng</a> </li>
                                <li><span class="icon"><i class="fa fa-question-circle"></i></span><a href="#">Tư vấn</a> </li>
                                </li>
            </div>
    </div>
            </div>
            <br></br>
            <div className="container-main">
                <div className="row">
                    <div className="col-sm-4">
                <div className="left-main"> 
                <h2 className="custom-name">Iphone 12 ProMax | Chính Hãng VN/A &emsp;
             <i className="fa fa-star checked"></i>
             <i className="fa fa-star checked"></i>
             <i className="fa fa-star checked"></i>
             <i className="fa fa-star checked"></i>
             <i className="fa fa-star checked"></i>
                528 lượt đánh giá
             </h2>
             <hr width="100%" align="all" color="grey" />
             
                                    <div className="custom-img">
                                <img src="1.jpg"alt=""/>
                                </div>       
                </div>
                </div>
                <div className="col-sm-4">
                <div className="center-main">
                <p>Mua hàng từ :  &emsp;
                                        <select className="custom-option" width="100%" height="100%">
                                            <option>Thành Phố Hồ Chí Minh</option>
                                            <option>Thành Phố Đà Nẵng</option>
                                            <option>Thành Phố Tây Ninh </option>
                                            <option>Hà Nội</option>
                                            <option>Thành Phố Cần Thơ</option>
                                        </select>
                                    </p>
                                     <div className="custom-pro">
                                        <p className="custom-price">35.000.000 ₫ 
                                        <p className="p-custom">Giá niêm yết:&nbsp;
                                    <span className="price-change">38.000.000&nbsp;₫</span>
                                    <div className="pay">
                                    <Link to="thanhtoan"> Trả góp 0% </Link>
                                    </div>
                                    </p>
                                    </p>
                                    </div> 
                                    <br></br>
                                    <div className="linked">
                                    <a className="item" href="#" title="iPhone 12 Pro Max 512GB I Chính hãng VN/A " >
                                <span><i className="sup-color"></i>512GB</span>
                                <strong>38.500.000&nbsp;₫</strong>
                            </a>
                                    <a className="item" href="#" title="iPhone 12 Pro Max 256GB I Chính hãng VN/A " >
                                <span><i className="sup-color"></i>256GB</span>
                                <strong>35.500.000&nbsp;₫</strong>
                            </a>
                                    <a className="item" href="#" title="iPhone 12 Pro Max I Chính hãng VN/A " >
                                <span><i className="sup-color"></i>128GB</span>
                                <strong>36.000.000&nbsp;₫</strong>
                            </a>
			                </div>
                            <p>Chọn màu điện thoại và giá kèm theo</p>
                            <div className="linked-custom">
                                    <a className="item" href="#" title="iPhone 12 Pro Max 512GB I Chính hãng VN/A " >
                                <span><i className="sup-color"></i>Màu vàng</span>
                                <strong>38.500.000&nbsp;₫</strong>
                            </a>
                                    <a className="item" href="#" title="iPhone 12 Pro Max 256GB I Chính hãng VN/A " >
                                <span><i className="sup-color"></i>Màu bạc</span>
                                <strong>35.500.000&nbsp;₫</strong>
                            </a>
                                    <a className="item" href="#" title="iPhone 12 Pro Max I Chính hãng VN/A " >
                                <span><i className="sup-color"></i>Màu xám</span>
                                <strong>36.000.000&nbsp;₫</strong>
                            </a>
                            <a className="item" href="#" title="iPhone 12 Pro Max I Chính hãng VN/A " >
                                <span><i className="sup-color"></i>Màu xanh dương</span>
                                <strong>37.000.000&nbsp;₫</strong>
                                </a>
			                </div>
                            <div className="info-khmai">
                            <aside class="promotion_wrapper">
                    <b id="promotion_header"><i class="fa fa-gift" aria-hidden="true"></i> Khuyến mãi</b>
                    <div class="khuyenmai-info">
                        <div class="kmChung"><h4>Khuyến mãi hãng:</h4><div class="pack-detail">
                            <ul class="el-promotion-pack" >
                               <a href="#">[HOT] Thu cũ lên đời giá cao - Thủ tục nhanh - Trợ giá lên tới 1 triệu</a></ul></div></div>
                    </div>
                </aside>
                </div>
                <i className="fa fa-shopping-cart"><a href="#">Thanh toán Online | Miễn phí giao hàng thu tiền</a></i><br></br>
                {/* <strong>
                    Mua ngay
                </strong>
                <br></br> (Giao hàng tận nơi hoặc xem và lấy trực tiếp tại cửa hàng) */}
                 {/* <Button className="btn-lg btn-dark btn-block">Mua ngay
                 <br></br>(Giao hàng tận nơi hoặc xem và lấy trực tiếp tại cửa hàng)
                 </Button> */}
                 <Link to ="/shopcart">
									 <a class="big-button button-red left" onclick="addToCart()">
										<strong>MUA NGAY</strong>
										<br></br>
										(Giao tận nơi hoặc lấy tại cửa hàng)	
                                        </a>							
                                    </Link>
                                    </div>   
			                </div>
                                                
                
                
                <div className="col-sm-4">
                <div className="right-main">
                <h5>HIỆN CÒN 25 CỬA HÀNG CÒN SẢN PHẨM </h5>
                                    <select className="custom-option" width="100%" height="100%">
                                            <option>Quận/Huyện</option>
                                            <option>Quận 1</option>
                                            <option>Quận 2</option>
                                            <option>Quận 3</option>
                                            <option>Quận 4</option>
                                            <option>Quận 7</option>
                                            <option>Quận 8</option>
                                            <option>Quận 9</option>
                                            <option>Quận 10</option>
                                            <option>Quận 12</option>
                                            <option>Quận Gò Vấp</option>
                                            <option>Quận Phú Nhuận</option>
                                            <option>Quận Bình Tân</option>
                                            <option>Quận Tân bình</option>
                                            <option>Huyện Dương Minh Châu </option>
                                            <option>Quận Đống Đa</option>
                                            <option>Quận Nink Kiều</option>
                                        </select>
                                        <br></br><br></br>
                                  <p>Tình trạng</p>
                                 <a> Máy mới 100%, chính hãng Apple Việt Nam</a>
                                 <a> My AppStore hiện đang là đại lí bán lẻ ủy quyền chính hãng VN/A của Apple Việt Nam</a> 
                                 <p>Hộp bao gồm</p>
                                 <a> Thân máy cáp sạc, hộp dây tai nghe, khe chọt sim...</a>                           
                                 <p>Bảo hành </p>
                                 <a>Bảo hành 12 tháng tại trung tâm bảo hành chính hãng của Apple Việt Nam. 1 đổi 1 trong thời gian 30 ngày nếu có lỗi từ nhà sản xuất</a>
                </div>
            </div>
            </div>
            <hr width="100%" align="all" color="grey" />
            <div className="row">
                <div className="col-sm-8">
                    <div className="detail-info">
                    <h3>Điện Thoại IPhone 12 ProMax: Nâng Tầm Đẳng Cấp Sử Dụng</h3>
                                         <p>Cứ mỗi năm, đến dạo cuối tháng 8 và gần đầu tháng 9 thì mọi thông tin sôi sục mới về chiếc iPhone mới lại xuất hiện. Apple năm nay lại ra thêm một chiếc iPhone mới với tên gọi mới là iPhone 12 Pro Max, đây là một dòng điện thoại mới và mạnh mẽ nhất của nhà Apple năm nay. Mời bạn tham khảo thêm một số mô tả sản phẩm bên dưới để bạn có thể ra quyết định mua sắm.</p>
                                    <h3>Màn hình 6.7 inches Super Retina XDR</h3>
                                     <p>Năm nay, công nghệ màn hình trên 12 Pro Max cũng được đổi mới và trang bị tốt hơn cùng kích thước lên đến 6.7 inch, lớn hơn so với điện thoại iPhone 12. Với công nghệ màn hình OLED cho khả năng hiển thị hình ảnh lên đến 2778 x 1284 pixels. Bên cạnh đó, màn hình này còn cho độ sáng tối đa cao nhất lên đến 800 nits, luôn đảm bảo cho bạn một độ sáng cao và dễ nhìn nhất ngoài nắng.</p>
                                     
                       
                                     <h3>Các Video đánh giá sản phẩm</h3>
                                      <div className="video-detail">
                                          <div className="row">
                                          {/* <div className="col-sm-8"> */}
                                              <div className="colsm-2">
                                          <li>
                                          <div class="img-section" data-id="Pbnm8NVjp-A">
				<a href="javascript:void(0)" onclick="videoFirst.showAllVideo('Pbnm8NVjp-A')" data-featherlight="iframe" data-featherlight-iframe-allowfullscreen="true" data-featherlight-iframe-width="500" data-featherlight-iframe-height="281">						
						<img class="cpslazy loaded" data-src="https://img.youtube.com/vi/Pbnm8NVjp-A/mqdefault.jpg" data-ll-status="loaded" src="https://img.youtube.com/vi/Pbnm8NVjp-A/mqdefault.jpg"/>
						<span><i class="fa fa-play"></i></span>
					</a>					
				</div>
            
            
				<div class="info-section">
					<h4 className="custom-video">LÍ DO KHIẾN MÌNH QUYẾT ĐỊNH MUA iPHONE 12 PRO MAX!!!</h4>
				</div>
                                          </li>
                                          <li>
                                          <div className="colsm-2">
                                          <div class="img-section" data-id="tMB0siZGh8Y">
				<a href="javascript:void(0)" onclick="videoFirst.showAllVideo('tMB0siZGh8Y')" data-featherlight="iframe" data-featherlight-iframe-allowfullscreen="true" data-featherlight-iframe-width="500" data-featherlight-iframe-height="281">						
						<img class="cpslazy loaded" data-src="https://img.youtube.com/vi/tMB0siZGh8Y/mqdefault.jpg" data-ll-status="loaded" src="https://img.youtube.com/vi/tMB0siZGh8Y/mqdefault.jpg"/>
						<span><i class="fa fa-play"></i></span>
					</a>	
                    </div>				
				</div>
                <div class="info-section">
					<h4 className="custom-video">VLOG TEST PIN iPHONE 12 PRO MAX: CHƯA ĐÃ BẰNG 11 PRO MAX…!!! (KHÁ TIẾC)</h4>
				</div>
                                          </li>
                                          <li>
                                          <div className="colsm-2">
                                          <div class="img-section" data-id="w7aLS0rsh58">
				<a href="javascript:void(0)" onclick="videoFirst.showAllVideo('w7aLS0rsh58')" data-featherlight="iframe" data-featherlight-iframe-allowfullscreen="true" data-featherlight-iframe-width="500" data-featherlight-iframe-height="281">						
						<img class="cpslazy loaded" data-src="https://img.youtube.com/vi/w7aLS0rsh58/mqdefault.jpg" data-ll-status="loaded" src="https://img.youtube.com/vi/w7aLS0rsh58/mqdefault.jpg"/>
						<span><i class="fa fa-play"></i></span>
					</a>	
                    </div>				
				</div>
                <div class="info-section">
					<h4 className="custom-video">Bên trong kho chứa 100,000 iPhone 12 tại Việt Nam !</h4>
				</div>
                                          </li>
                                          <li>
                                          <div className="colsm-2">
                                          <div class="img-section" data-id="5WQONbuGWeo">
				<a href="javascript:void(0)" onclick="videoFirst.showAllVideo('5WQONbuGWeo')" data-featherlight="iframe" data-featherlight-iframe-allowfullscreen="true" data-featherlight-iframe-width="500" data-featherlight-iframe-height="281">						
						<img class="cpslazy loaded" data-src="https://img.youtube.com/vi/5WQONbuGWeo/mqdefault.jpg" data-ll-status="loaded" src="https://img.youtube.com/vi/5WQONbuGWeo/mqdefault.jpg"/>
						<span><i class="fa fa-play"></i></span>
					</a>	
                    </div>				
				</div>
                                          </li>
                                      </div>    
                                      </div>
                                      </div>  
                                      <div className="row">
                                          <div className="col-sm-6">
                                          <h2 className="custom-id">Iphone 12 ProMax | Chính Hãng VN/A &emsp;</h2>
                                      <div className="custom-pro">
                                        <p className="custom-chang">35.000.000 ₫ &emsp;
                                        <p className="p-custom">Giá niêm yết:&nbsp;
                                    <span className="price-cdo">38.000.000&nbsp;₫</span>
                                    <div className="pay">
                                    <Link to="thanhtoan"> Trả góp 0% </Link>
                                    </div>
                                    </p>
                                    </p>
                                    </div>  
                                          </div>
                                          <div className="col-sm-6">
                                          <a class="big-button button-red left" onclick="addToCart()">
										<strong>MUA NGAY</strong>
										<br></br>
										(Giao tận nơi hoặc lấy tại cửa hàng)	
                                        </a>
                                        <div class="f-left">
										  <a class="big-button button-blue" href="#" >
                                            <strong>TRẢ GÓP 0%</strong>
                                            <br></br>
                                            (Xét duyệt qua điện thoại)
                                        </a>
										 <a class="big-button button-blue" onclick="tragop()">
                                            <strong>TRẢ GÓP QUA THẺ</strong>
                                            <br></br>
                                            (Visa, Master, JCB)
                                        </a>
									  </div>
                                          </div>
                                          <h6 className="custom-h6">Các sản phẩm tương tự</h6>
                                          <div className="container-img">
                                <a href=""><img src="17.jpg"alt=""/>SamSung Galaxy Note 20 Ultra 5G</a>
                                <div className="change-price">25.000.000 VNĐ</div>
                                <p className="change-custom">Giá niêm yết:&nbsp;
                                    <span className="change-cdo">22.000.000&nbsp;₫</span>
                                    </p></div>
                                    <div className="container-img">
                                    <a href=""><img src="15.jpg"alt=""/>SamSung Galaxy S21 Plus 5G</a>
                                <div className="change-price">25.000.000 VNĐ</div>
                                <p className="change-custom">Giá niêm yết:&nbsp;
                                    <span className="change-cdo">22.000.000&nbsp;₫</span>
                                    </p>
                                    </div>
                                    <div className="container-img">
                                    <a href=""><img src="21.jpg"alt=""/>Xiaomi Mi 10T Pro 5G</a>
                                <div className="change-price">12.000.000 VNĐ</div>
                                <p className="change-custom">Giá niêm yết:&nbsp;
                                    <span className="change-cdo">15.000.000&nbsp;₫</span>
                                    </p>
                                    </div>
                                    <div className="container-img">
                                    <a href=""><img src="25.jpg"alt=""/>Vsmart Live 4</a>
                                <div className="change-price">3.800.000 VNĐ</div>
                                <p className="change-custom">Giá niêm yết:&nbsp;
                                    <span className="change-cdo">4.000.000&nbsp;₫</span>
                                    </p>
                                </div>
                                <div className="container-img">
                                    <a href=""><img src="14.jpg"alt=""/>Huawie Y6p</a>
                                <div className="change-price">10.000.000 VNĐ</div>
                                <p className="change-custom">Giá niêm yết:&nbsp;
                                    <span className="change-cdo">12.000.000&nbsp;₫</span>
                                    </p>
                                </div>
            </div>
                    
  
                                      {/* <h2 className="custom-name">Iphone 12 ProMax | Chính Hãng VN/A &emsp;</h2>
                                      <div className="custom-pro">
                                        <p className="custom-price">35.000.000 ₫ 
                                        <p className="p-custom">Giá niêm yết:&nbsp;
                                    <span className="price-change">38.000.000&nbsp;₫</span>
                                    <div className="pay">
                                    <Link to="thanhtoan"> Trả góp 0% </Link>
                                    </div>
                                    </p>
                                    </p>
                                    </div>  */}
                    <div className="star-danhgia">* NOTE: Hãy nhấn vào sao để đánh giá sản phẩm chất lượng của cửa hàng</div>
                                    <textarea class="form-control wmd-input autoresize" id="wmd-input"  placeholder="Nhập bình luận..."  name="content" ></textarea>
                                    <button className="submit-content">Gửi bình luận</button><br></br>
                                    <div className="content-fb">Bình luận Facebook
                                    <div aria-autocomplete="list" aria-controls="js_0" aria-describedby="placeholder-vt4d" aria-expanded="false" aria-label="Thêm bình luận" class="notranslate _5rpu" contenteditable="true" role="combobox" spellcheck="false" ></div>
                                    <button>Đăng bình luận</button><br></br>
                                    <a target="_blank" href="https://developers.facebook.com/products/social-plugins/comments/?utm_campaign=social_plugins&amp;utm_medium=offsite_pages&amp;utm_source=comments_plugin">Plugin bình luận trên Facebook</a>
                                </div>
                                <br></br>
                                <h4 className="custom-h4">HÃY TƯƠNG TÁC MẠNH MẼ ĐỂ GIA ĐÌNH MY APPSTORE LỚN MẠNH</h4>
                                <a id="cmt_loadmore" href="#" class="btn btn-default btn-sm" onclick="comment.loadMore()">Xem thêm</a>
                    </div>
                
                </div>
                <div className="col-sm-4">
                 <div className="detail-parameter">
                 <h2>Thông số kỹ thuật</h2>
                                     </div>
                                     <div className="content">
                                     <table id="tskt">
                                     <tbody>
                                     <tr >
                                     <td>Kích thước màn hình</td>
					                    <td>6.7 inches</td>
                                     </tr>
                                     <hr width="600%" align="all" color="grey" />
                                     <tr >
                                    <td>Công nghệ màn hình</td>
                                    <td>OLED</td>
                                </tr>
                                <hr width="600%" align="all" color="grey" />
                                <tr >
                                <td>Camera sau</td>
					<td>12 MP, f/1.6, 26mm (wide), 1.4µm, dual pixel PDAF, OIS<br></br> 12 MP, f/2.0, 52mm (telephoto), 1/3.4", 1.0µm, PDAF, OIS, 2x optical zoom<br></br> 12 MP, f/2.4, 120˚, 13mm (ultrawide), 1/3.6"<br></br> TOF 3D LiDAR scanner (depth)</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
                <tr >
					<td>Chipset</td>
                    
					<td>Apple A14 Bionic (5 nm)</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
                <tr>
					<td>Dung lượng RAM</td>
					<td>6 GB</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
                <tr>
					<td>Bộ nhớ trong</td>
					<td>128 GB</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
                <tr>
					<td>Pin</td>
					<td>Li-Ion, sạc nhanh 20W, sạc không dây 15W, USB Power Delivery 2.0</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
		
        					<tr>
					<td>Thẻ SIM</td>
					<td>2 SIM (nano‑SIM và eSIM)</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
                <tr>
					<td>Hệ điều hành</td>
					<td>iOS 14.1 hoặc cao hơn (Tùy vào phiên bản phát hành)</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
		
        					<tr>
					<td>Độ phân giải màn hình</td>
					<td>1284 x 2778 pixels</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
            
		
        					<tr>
					<td>Tính năng màn hình</td>
					<td>HDR10<br></br>Dolby Vision<br></br>True-tone<br></br>Độ sáng 800 nits</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
		
        					<tr>
					<td>Loại CPU</td>
					<td>Hexa-core</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
		
        					<tr>
					<td>GPU</td>
					<td>Apple GPU (4-core graphics)</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
                <tr>
					<td>Kích thước</td>
					<td>160.8 x 78.1 x 7.4 mm</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
		
        					<tr>
					<td>Trọng lượng</td>
					<td>228 </td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Quay video</td>
					<td>Trước: 4K@24/30/60fps, 1080p@30/60/120fps, gyro-EIS<br></br>Sau: 4K@24/30/60fps, 1080p@30/60/120/240fps, HDR, Dolby Vision HDR (up to 60fps), stereo sound rec.</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Quay video trước</td>
					<td>4K@24/30/60fps, 1080p@30/60/120fps, gyro-EIS</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Chất liệu mặt lưng</td>
					<td>Kính</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
            		<tr>
					<td>Chất liệu khung viền</td>
					<td>Kim loại</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Công nghệ sạc</td>
					<td>Sạc nhanh 20W<br></br>Sạc không dây 15W<br></br>Power Delivery 2.0</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Cổng sạc</td>
					<td>Lightning</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Công nghệ NFC</td>
					<td>Có</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Hồng ngoại</td>
					<td>Không</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Jack tai nghe 3.5</td>
					<td>Không</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Cảm biến vân tay</td>
					<td>Không</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Các loại cảm biến</td>
					<td>Cảm biến ánh sáng, Cảm biến áp kế, Cảm biến gia tốc, Cảm biến tiệm cận, Con quay hồi chuyển, La bàn</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Khe cắm thẻ nhớ</td>
					<td>Không</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Wi-Fi</td>
					<td>Wi-Fi 802.11 a/b/g/n/ac/6, dual-band, hotspot</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Bluetooth</td>
					<td>5.0, A2DP, LE</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>GPS</td>
					<td> A-GPS, GLONASS, GALILEO, QZSS</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Kiểu màn hình</td>
					<td>Tai thỏ</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Tính năng camera</td>
					<td>Chụp góc rộng, Chụp xóa phông, Chụp Zoom xa, Chống rung, Quay video 4K</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
        					<tr>
					<td>Tính năng đặc biệt</td>
					<td>Sạc không dây</td>
				</tr>
                <hr width="600%" align="all" color="grey" />
                </tbody>
                                     </table>
                 </div>
                </div>
            </div>
            </div>
            <div className="footer">
            <div classNameName="footer-form">
            <div classNameName="col-md-12 footer__box-contact bg-white">
                <div className="container-footer">
                <div className="row py-4">
                <div className="col-md-3 col-sm-3 col-xs-3">
                <div className="row">
                <ul className="list-unstyled m-0">
                 <li className="mb-3"> 
                <p className="font-16 mb-2"><label>Địa chỉ cửa hàng</label></p>
                <p className="mb-0"><a className="test-text" href="#">Tìm cửa hàng gần nhất</a></p>
                <p className="mb-0"><a className="test-text" href="#">Mua hàng Online</a></p>
                <div className="box-cttt">
                <p className="font-16 mb-0"><label>Phương thức thanh toán</label></p>
                <ul className="col-md-10 col-sm-10  col-sm-10 d-flex flex-wrap m-0 list-unstyled">
                {/* <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-visa"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mastercard"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-atm"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-alepay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-zalopay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-vnpay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-momo"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mpos"></em></a></li> */}
                <span class="icon"><i class="fa fa-cc-visa"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-mastercard"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-paypal"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-jcb"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-credit-card-alt"></i></span><a href="#"></a>
                </ul>
                </div>
                 </li> 
                </ul>
                </div>
              
                </div>
                <div className="col-md-3 col-sm-3 col-xs-3" >
                    <div className="row">
                    <ul className="list-unstyled m-0">
                    <p className="font-16 mb-0"><label>Dịch vụ hỗ trợ tư vấn khách hàng</label></p>
                    {/* <li className="mb-4"> */}
                    {/* <p className="mb-0">Liên hệ mua hàng: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 22h00)</p><br></br>
                    <p className="mb-0">Liên hệ bảo hành: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 21h00)</p> */}
                    <p>Liên hệ mua hàng- Hotline :1800.2020<br></br>Thời gian làm việc: 24/24</p>
                    <p>Liên hệ bảo hành- Hotline :1800.2020 <br></br>Thời gian làm việc :8h AM - 12h PM</p>
                    {/* </li> */}
                    <li>
                   </li>
                   </ul>
                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Mua hàng và thanh toán Online</p>
                        <p>Mua hàng và thanh toán trả góp qua Online</p>
                        <p>Tra cứu thông tin đơn hàng</p>
                        <p>Tra cứu thông tin bảo hành sản phẩm</p>
                        <p>Trung tâm bảo hành chính hãng</p>
                        <p>Dịch vụ bảo hành và sưa chữa điện thoại</p>

                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Quy chế hoạt động</p>
                        <p>Chính sách bảo hành</p>
                        <p>Điều kiện chính sách</p>
                        <p>Thông tin pháp lý</p>
                        <p>Giới thiệu cửa hàng</p>
                        <p>Giấy phép kinh doanh</p>

                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            </div>
                </div>
        )
    }
}
export default DetailProduct;