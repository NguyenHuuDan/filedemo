import React, {Component, Components} from 'react';
import { Input,Label } from 'reactstrap';

class ShopCart extends Component{
    render(){
        return(
            <div>
                  <div className="root">
                    <div className="header">
                    <div className="header-form">
                       <h2 className="custom-h2">MyAppStore- World Phone</h2>
                <div className="col-md-12 navbar-cps_box-main">
                                <li className="box-man"><a href="#">Trang chủ MyAppStore</a>
                             <li>  <a href=""><img src="apple.jpg"alt="" width="100px" height="100px" /> </a> </li> 
                                    <li><a href="#">Điện thoại  <span className="arrow">
                                    {/* <select multiple className="lua-chon">
                                        <option value="0">Chọn sản phẩm điện thoại bạn cần </option>
                                        <option value="34">Iphone </option>
                                        <option value="46">Samsung </option>
                                        <option value="68">Oppo </option>
                                        <option value="812">Vivo</option>
                                        </select> */}
                                        </span></a></li>
                                    <li>
                                        <input type="search" name="search" placeholder="Tìm kiếm sản phẩm..." />
                                        <span class="icon"><i class="fa fa-search"></i></span></li>
                                        {/* <ul className="form-menu">
                                        <li><a href="#" >Iphone</a></li>
                                        <li><a href="#" >Samsung</a></li>
                                        <li><a href="#" >Oppo</a></li>
                                        <li><a href="#" >Vivo</a></li>
                                        </ul> */}
                                      
                                        <li><span class="icon"><i class="fa fa-phone"></i></span><a href="#">Liên hệ</a></li> 
                                        <li><span class="icon"><i class="fa fa-shopping-cart"></i></span><a href="#">Giỏ hàng</a></li>
                                        <li><span class="icon"><i class="fa fa-cogs"></i></span><a href="#">Chăm sóc khách hàng</a> </li>
                                        <li><span class="icon"><i class="fa fa-question-circle"></i></span><a href="#">Tư vấn</a> </li>
                                        </li>
                    </div>
            </div>
                    </div>
                    </div>
                    <div className="container-main">
                        <div className="container-shopcart">
                            <div className="left">
                            <div className="custom-img">
                                <img src="1.jpg"alt=""/>
                                </div>
                                
                            </div>
                            <div className="right">
                            <h2 className="custom-name">Iphone 12 ProMax | Chính Hãng VN/A</h2>
                             <span className="custom-price">35.000.000 ₫ </span>
                             <p>-Khuyến mãi hãng:</p>
                             <ul>* [HOT] Thu đổi dòng máy cũ sang dòng máy mới- Thủ tục nhanh -Trợ giá lên đến 1 triệu đồng</ul>
                             <a href="#" >Xóa sản phẩm khỏi giỏ</a>
                             {/* <ul className="soluong-product"> */}
                                        <button className="click-sl"onClick="">-</button>
                                        <button className="click-sl"onClick="">0</button>
                                        <button className="click-sl"onClick="">+</button>
                                     {/* </ul> */}
                            </div>
                            <hr width="70%" align="all" color="grey" />
                            
                            
                           
                                <div className="infom-kmai">
                                <p>-Nhập mã khuyến mãi:</p>
                                 <Input type="" name="km" placeholder="" width="70%">- Nhập mã khuyến mãi  </Input>
                                 <button>Áp dụng ngay</button>
                                 </div>
                                 <br>
                                 </br>
                                 <br></br>
                                 <hr width="70%" align="all" color="grey" />
                                 
                                 <h6 className="char-name">Thông tin mua hàng</h6>
                                 <div className="container-info">
                                    <div className="row">
                                    <div className="col-sm-4">
                                    <Label>Họ và tên(bắt buộc)</Label>
                        <Input type="fullname" name="fullname" placeholder=""width="50%" ></Input>
                                    </div>
                                    <div className="col-sm-4">
                                    <Label>Số điện thoại đặt hàng(bắt buộc)</Label>
                        <Input type="numberphone" name="numberphone" placeholder="" width="50%"></Input>  
                                    </div>
                                    <div className="col-sm-8">
                                    <div className="note">
                                    <Label>Lưu ý</Label>
                        <Input type="note-gc" name="note-gc" placeholder="" width="80%"></Input>
                        </div>
                        </div>
                        </div>
                        <h6 className="char-name">-Vui lòng chọn cách thức nhận hàng</h6>
                        <div className="check-pt">
                            <div className="row">
                                <div className="col-sm-4"> <label>
                        <input type="checkbox" />&emsp;
                            Giao nhận tại cửa hàng</label></div>
                                <div className="col-sm-4">  <label>
                        <input type="checkbox" />&emsp;
                            Giao hàng tận nơi</label></div>
                        {/* <label>
                        <input type="checkbox" />
                            Giao nhận tại cửa hàng</label>
                            <label>
                        <input type="checkbox" />
                            Giao hàng tận nơi</label> */}
                            </div>
                        </div>
                      <div className="col-sm-8">
                      <a onclick="submitForm('cashondelivery')" class="big-button button-blue" >
                            <strong>ĐẶT HÀNG THANH TOÁN SAU</strong> <br></br>
                    (Trả tiền tại nhà hoặc tại cửa hàng)
                </a>
                      </div>
                      <br>
                      </br>
                      <h4>HOẶC CHỌN THANH TOÁN ONLINE </h4>
                      <div className="thanh-toan">
                      <div id="zalopay"><input name="payment-method" type="radio" id="zalopay-method" value="zalopay"/> Cổng thanh toán ZaloPay  <label for="zalopay-method" >
                          <i class="cps-zalopay"></i>
                     <i class="cps-atm"></i><i class="cps-visa"></i><i class="cps-master"></i></label></div><br></br>
                     </div>
                     <div className="col-sm-8">
                     <div id="vnpay"><input name="payment-method" type="radio" id="vnpay-method" value="vnpay"/>  Thanh toán qua VNPAY <label for="vnpay-method" >
                         <i class="cps-vnpay"></i>
                        <a href="#"></a> <i class="cps-atm"></i></label></div>
                        {/* target="_blank">(Chi tiết */}
                     </div>
                        </div>
                        <div className="online">
                      <h5> THÔNG QUA ONLINE </h5>
                    (Thanh toán bằng thẻ)
                    </div>
                    <div className="col-sm-7">
                    <a id="online-method" onclick="paymentMethods()" class="big-button button-red"><strong>THANH TOÁN ONLINE</strong> <br></br>
                    (Thẻ Visa, Master, ATM, Momo…)
                </a>
                </div>
                        </div>
                        {/* <a class="big-button button-red left" onclick="addToCart()">
										<strong>THANH TOÁN ONLINE</strong>
										<br></br>
										(Thẻ Visa, ATM, MoMo, Master...)	
                                        </a>	  */}
                   {/* <div className="online">
                       THÔNG QUA ONLINE <br></br>
                    (Thanh toán bằng thẻ)
                    </div>
                    <div className="col-sm-8">
                    <a id="online-method" onclick="paymentMethods()" class="big-button button-red"><strong>THANH TOÁN ONLINE</strong> <br></br>
                    (Thẻ Visa, Master, ATM, Momo…)
                </a>
                </div> */}
                    </div>
                    <div className="footer">
                    <div classNameName="footer-form">
            <div classNameName="col-md-12 footer__box-contact bg-white">
                <div className="container-footer">
                <div className="row py-4">
                <div className="col-md-3 col-sm-3 col-xs-3">
                <div className="row">
                <ul className="list-unstyled m-0">
                 <li className="mb-3"> 
                <p className="font-16 mb-2"><label>Địa chỉ cửa hàng</label></p>
                <p className="mb-0"><a className="test-text" href="#">Tìm cửa hàng gần nhất</a></p>
                <p className="mb-0"><a className="test-text" href="#">Mua hàng Online</a></p>
                <div className="box-cttt">
                <p className="font-16 mb-0"><label>Phương thức thanh toán</label></p>
                <ul className="col-md-10 col-sm-10  col-sm-10 d-flex flex-wrap m-0 list-unstyled">
                {/* <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-visa"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mastercard"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-atm"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-alepay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-zalopay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-vnpay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-momo"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mpos"></em></a></li> */}
                <span class="icon"><i class="fa fa-cc-visa"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-mastercard"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-paypal"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-jcb"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-credit-card-alt"></i></span><a href="#"></a>
                </ul>
                </div>
                 </li> 
                </ul>
                </div>
              
                </div>
                <div className="col-md-3 col-sm-3 col-xs-3" >
                    <div className="row">
                    <ul className="list-unstyled m-0">
                    <p className="font-16 mb-0"><label>Dịch vụ hỗ trợ tư vấn khách hàng</label></p>
                    {/* <li className="mb-4"> */}
                    {/* <p className="mb-0">Liên hệ mua hàng: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 22h00)</p><br></br>
                    <p className="mb-0">Liên hệ bảo hành: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 21h00)</p> */}
                    <p>Liên hệ mua hàng- Hotline :1800.2020<br></br>Thời gian làm việc: 24/24</p>
                    <p>Liên hệ bảo hành- Hotline :1800.2020 <br></br>Thời gian làm việc :8h AM - 12h PM</p>
                    {/* </li> */}
                    <li>
                   </li>
                   </ul>
                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Mua hàng và thanh toán Online</p>
                        <p>Mua hàng và thanh toán trả góp qua Online</p>
                        <p>Tra cứu thông tin đơn hàng</p>
                        <p>Tra cứu thông tin bảo hành sản phẩm</p>
                        <p>Trung tâm bảo hành chính hãng</p>
                        <p>Dịch vụ bảo hành và sưa chữa điện thoại</p>

                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Quy chế hoạt động</p>
                        <p>Chính sách bảo hành</p>
                        <p>Điều kiện chính sách</p>
                        <p>Thông tin pháp lý</p>
                        <p>Giới thiệu cửa hàng</p>
                        <p>Giấy phép kinh doanh</p>

                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                 </div>
        )
    }
}
export default ShopCart;