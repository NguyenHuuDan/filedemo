import React, {Component, Components} from 'react';
import { Input,Label } from 'reactstrap';

import Slider from "react-slick";

class Home extends Component{
    render(){
        
            const settings = {
              dots: true,
              infinite: true,
              speed: 500,
              slidesToShow: 5,
              slidesToScroll: 1,
              className:"slides"
            };
        return(
            <div>
            <div className="root">
              <div className="header">
              <div className="header-form">
                 <h2 className="custom-h2">MyAppStore- World Phone</h2>
          <div className="col-md-12 navbar-cps_box-main">
                          <li className="box-man"><a href="#">Trang chủ MyAppStore</a>
                       <li>  <a href=""><img src="apple.jpg"alt="" width="100px" height="100px" /> </a> </li> 
                              <li><a href="#">Điện thoại  <span className="arrow">
                              {/* <select multiple className="lua-chon">
                                  <option value="0">Chọn sản phẩm điện thoại bạn cần </option>
                                  <option value="34">Iphone </option>
                                  <option value="46">Samsung </option>
                                  <option value="68">Oppo </option>
                                  <option value="812">Vivo</option>
                                  </select> */}
                                  </span></a></li>
                              <li>
                                  <input type="search" name="search" placeholder="Tìm kiếm sản phẩm..." />
                                  <span class="icon"><i class="fa fa-search"></i></span></li>
                                  {/* <ul className="form-menu">
                                  <li><a href="#" >Iphone</a></li>
                                  <li><a href="#" >Samsung</a></li>
                                  <li><a href="#" >Oppo</a></li>
                                  <li><a href="#" >Vivo</a></li>
                                  </ul> */}
                                
                                  <li><span class="icon"><i class="fa fa-phone"></i></span><a href="#">Liên hệ</a></li> 
                                  <li><span class="icon"><i class="fa fa-shopping-cart"></i></span><a href="#">Giỏ hàng</a></li>
                                  <li><span class="icon"><i class="fa fa-cogs"></i></span><a href="#">Chăm sóc khách hàng</a> </li>
                                  <li><span class="icon"><i class="fa fa-question-circle"></i></span><a href="#">Tư vấn</a> </li>
                                  </li>
              </div>
                </div>
              </div>
              </div>
              <div className="container-main">
                {/* <div className="row">
                    <div className="col-sm-3">
                        <div className="banner-menu">

                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="banner-main"></div>
                    </div>
                    <div className="col-sm-3">
                        <div className="banner-right"></div>
                    </div>
                </div> */}
                <div className="banner">
                    <div className="row">
                        <div className="col-md-12 mb-4">
                            <div className="row">
                            <div class="col-md-3 col-sm-3 col-xs-3 box-banner__left">
                            <div class="row pr-4">
                                <ul className="col-md-12 box-listmenu">
                                <li className="menu-items">
                                    <a href="#">
                                        <i className="icon-cps-3"></i>
                                        <span>Điện Thoại</span>&emsp;&emsp;
                                        <i className="fa fa-angle-right"></i>
                                    </a>
                                    <ul className="box-listmenu box-menufirst">
                                <li className="menu-items">
                                    <a href="#">
                                        <span>Apple</span>
                                    
                                        <i className="fa fa-angle-right"></i>
                                    </a>
                                    <ul className="box-listmenu box-menufirst">
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Iphone 12 Series</span>
                                    </a>
                                    </li> 
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Iphone 11 Series</span>
                                    </a>
                                    </li> 
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Iphone X | XR</span>
                                    </a>
                                    </li> 
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Iphone SE 2020</span>
                                    </a>
                                    </li> 

                                    </ul>
                                </li>
                                <li className="menu-items">
                                      <a href="#">
                                        <span>SamSung</span>
                                    
                                    </a>
                                    </li>
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Xiaomi</span>
                                    
                                      
                                    </a>
                                    </li>
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Oppo</span>
                                    
                                       
                                    </a>
                                    </li>
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Vivo</span>
                                    
                                       
                                    </a>
                                    </li>
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Vsmart</span>
                                    
                                      
                                    </a>
                                    </li>
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Huawie</span>
                                    
                                       
                                    </a>
                                    </li>
                                    <li className="menu-items">
                                    <a href="#">
                                        <span>Nokia</span>
                                       
                                    </a>
                                    </li>
                                    </ul>
                                </li>
                            <li className="menu-items">
                                <a href="#">
                                   <i className="icon-cps-380"></i>
                                   <span>Laptop</span>&emsp;&emsp;
                                   <i className="fa fa-angle-right"></i>
                                </a>
                                <ul className="box-listmenu box-menufirst">
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>MAC</span>
                                            <i className="fa fa-angle-right"></i>
                                        </a>
                                        <ul className="box-listmenu box-menufirst">
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>MacBook Air</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>MacBook Pro</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>I Mac</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>MacBook Mini</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="menu-items">
                                                <a href="#">
                                                    <span>HP</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Dell</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Asus</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Lenovo</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Acer</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>MacBook Air</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>LG</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>MSI</span>
                                                </a>
                                            </li>
                                </ul>
                            </li>
                            <li className="menu-items">
                                <a href="#">
                                   <i className="icon-cps-4"></i>
                                   <span>Tablet</span>&emsp;&emsp;
                                   <i className="fa fa-angle-right"></i>
                                </a>
                                <ul className="box-listmenu box-menufirst">
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Ipad Pro</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Ipad 10.2</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Ipad Air</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Ipad Pro</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Samsung</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li className="menu-items">
                                <a href="#">
                                   <i className="icon-cps-220"></i>
                                   <span>Âm Thanh</span>&emsp;&emsp;
                                   <i className="fa fa-angle-right"></i>
                                </a>
                                <ul className="box-listmenu box-menufirst">
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Loa</span>
                                            <i className="fa fa-angle-right"></i>
                                        </a>
                                        <ul className="box-listmenu box-menufirst">
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Loa Vi Tính</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Loa Bluetooth</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Loa Kẹo Kéo</span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Loa Ti Vi</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Tai Nghe</span>
                                            <i className="fa fa-angle-right"></i>
                                        </a>
                                        <ul className="box-listmenu box-menufirst">
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>
                                                        Tai nghe không dây
                                                    </span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>
                                                        Tai nghe Bluetooth
                                                    </span>
                                                </a>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>
                                                        Tai nghe chụp tai
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>
                            <li className="menu-items">
                                <a href="#">
                                   <i className="icon-cps-610"></i>
                                   <span>Đồng Hồ</span>&emsp;&emsp;
                                   <i className="fa fa-angle-right"></i>
                                </a>
                                <ul className="box-listmenu box-menufirst">
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Apple Watch</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Samsung</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Oppo</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Huawie</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li className="menu-items">
                                <a href="#">
                                   <i className="icon-cps-30"></i>
                                   <span>Phụ Kiện</span>&emsp;&emsp;
                                   <i className="fa fa-angle-right"></i>
                                </a>
                                <ul className="box-listmenu box-menufirst">
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Phụ kiện Apple</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Dán điện thoại | Laptop</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Ốp lưng | Bao Da | Hộp để Ipod pro</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Dây đeo đồng hồ | Dây đeo đồng hô Apple Watch... </span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Cốc sạc |Cáp sạc</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Pin dự phòng</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Camera</span>
                                            <i className="fa fa-angle-right"></i>
                                        </a>
                                        <ul className="box-listmenu box-menufirst">
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Camera hành trình</span>
                                                    <i className="fa fa-angle-right">
                                                
                                                </i>
                                                </a>
                                            <ul className="box-menulist box-menufirst">
                                        <li className="menu-items">
                                            <a href="#">
                                                <span>Gopro</span>
                                            </a>
                                        </li>
                                        <li className="menu-items">
                                            <a href="#">
                                                <span>Vietmap</span>
                                            </a>
                                        </li>
                                            </ul>
                                            </li>
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Camera An Ninh</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Chuột | Bàn phím vi tính</span>
                                            <i className="fa fa-angle-right"></i>
                                        </a>
                                        <ul className="box-listmenu box-menufirst">
                                            <li className="menu-items">
                                                <a href="#">
                                                    <span>Phụ kiện máy vi tính Laptop</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li className="menu-items">
                                <a href="#">
                                   <i className="icon-cps-tcdm"></i>
                                   <span>Thu hàng cũ</span>&emsp;&emsp;
                                   <i className="fa fa-angle-right"></i>
                                </a>
                                <ul className="box-listmenu box-menufirst"></ul>
                            </li>
                            <li className="menu-items">
                                <a href="#">
                                   <i className="icon-cps-123"></i>
                                   <span>Sim thẻ</span>&emsp;&emsp;
                                   <i className="fa fa-angle-right"></i>
                                </a>
                                <ul className="box-listmenu box-menufirst">
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Thẻ nạp điện thaoi | Card</span>
                                        </a>
                                    </li>
                                    <li className="menu-items">
                                        <a href="#">
                                            <span>Dịch vụ Sim</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                                </ul>
                            </div>
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-7 border-radius shadow-sm overflow-hidden box-banner__center">
                                <div className="row">
                                    <div className="wrapper-container">
                                        <div className="wrapper-banner">
                                        {/* <div className="slides.slick-next" tabindex="0" role="button" aria-label="Next slide">
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>  */}
                                        {/* <div className="slides.slick-prev" tabindex="0" role="button" aria-label="Previous slide">
                                          <i class="fa fa-chevron-left" aria-hidden="true"></i> 
                                             */}
                                            <div className="banner-img">
                                                <a href="#">
                                                <div className="slides.slick-prev" tabindex="0" role="button" aria-label="Previous slide">
                                             <i class="fa fa-chevron-left" aria-hidden="true"></i> 
                <Slider {...settings}>
           <div>
             <h3> <div className="custom-img">
                                 <img src="26.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-img">
                                 <img src="27.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-img">
                                 <img src="25.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3>4 <div className="custom-img">
                                 <img src="19.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-img">
                                 <img src="21.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-img">
                                 <img src="20.jpg"alt=""/>
                                 </div></h3>
           </div>
         </Slider>
         <div className="slides.slick-next" tabindex="0" role="button" aria-label="Next slide">
                                             <i class="fa fa-chevron-right" aria-hidden="true"></i> 
                                             </div>
                                             </div>
        </a>
      
                                </div>
                               
                            </div>
                            </div>
                        </div>
                        {/* <div className="col-md-12 mb-4">
                            <a href="#">
                                <img className="banner-imgright"  alt="Asus Zenbook"src="29.jpg"></img>
                            </a>
                        </div> */}
                    </div>
                    <div className="custom-banner">
                                 <img src="28.jpg"alt=""/>
                                 </div>
                </div>
              </div>
              </div>
        </div>
        <div className="container-mb-4 product-img">
            <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12 bg-white border-radius shadow-sm">
            <div className="row">
            <div className="box-title border-radius">
            <div className="col-md-3 col-sm-3 col-xs-3 box-title__title">
            <div className="row">
             <a href="#" className="text-reset text-decoration-none"><h2><strong>Điện thoại nổi bật nhất</strong></h2></a>
             </div>
                </div>
                <div className="col-md-9 col-sm-9 col-xs-9 box-title__tag">
            <div className="row">
            <a class="btn btn-default btn-sm" href="#">Apple</a>
            <a class="btn btn-default btn-sm" href="#">Samsung</a>
            <a class="btn btn-default btn-sm" href="#">Xiaomi</a>
            <a class="btn btn-default btn-sm" href="#">OPPO</a>
            <a class="btn btn-default btn-sm" href="#">Nokia</a>
            <a class="btn btn-default btn-sm" href="#">Realme</a>
            <a class="btn btn-default btn-sm" href="#">Vsmart</a>
            <a class="btn btn-default btn-sm" href="#">ASUS</a>
            <a class="btn btn-default btn-sm" href="#">Vivo</a>         
             <a href="#">Xem tất cả</a>
            </div>
                </div>
            </div>
            <div className="product-img">
                                 </div>  
                                   
                                    <div className="product">
                              <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                              <a href="#">
                        <img src="2.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-11%</p>
                            </div>
                    </a>
                              </div>
                              <div className="col-md-12 col-sm-12 col-xs-12 product__box-info">
                              <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">iPhone 11 Pro Max I Chính hãng VN/A </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                             18.900.000&nbsp;₫     </p>
                     <p class="old-price">25.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">Giảm thêm 7.090.000đ khi mua kèm Apple Watch Series 6</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;38&nbsp;đánh giá              
                              </div>
                              <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                              <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                              </div>
                                  </div>    
                                  <br></br>
                                  <div className="product">
                              <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                              <a href="#">
                        <img src="32.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-12%</p>
                            </div>
                    </a>
                              </div>
                              <div className="col-md-12 col-sm-12 col-xs-12 product__box-info">
                              <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">iPhone 12 Pro Max I Chính hãng VN/A </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                             20.200.000&nbsp;₫     </p>
                     <p class="old-price">22.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">Giảm thêm 500.000đ khi mua kèm Apple Watch Series 6</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;162&nbsp;đánh giá 
                              </div>
                              <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                              <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                              </div>
                                   </div>

                                   <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="1.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-9%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">iPhone 12 Pro Max I Chính hãng VN/A </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                             29.800.000&nbsp;₫     </p>
                     <p class="old-price">32.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">Giảm thêm 500.000đ khi mua kèm Apple Watch Series 6</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;120&nbsp;đánh giá
                                                </div>
                                 
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                        </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="33.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-13%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">iPhone 12 Mini I Chính hãng VN/A </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                            16.590.000&nbsp;₫     </p>
                     <p class="old-price">18.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">Giảm thêm 500.000đ khi mua kèm Apple Watch Series 6</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;180&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                        </div>                  
                                   </div>
                                    <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="34.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-13%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">iPhone XR 64GB </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                             12.000.000&nbsp;₫     </p>
                     <p class="old-price">14.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">Giảm thêm 500.000đ khi mua kèm Apple Watch Series 6</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;191&nbsp;đánh giá
                                                
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                        </div>
                                   </div> 
                                     <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="35.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-17%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">SamSung Galaxy Note 20</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                            24.800.000&nbsp;₫     </p>
                     <p class="old-price">29.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">[Từ 1/4-30/4] giảm 30% đồng hồ samsung active 2 khi mua kèm điện thoại samsung (có điều kiện kèm theo]</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;83&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                        </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="5.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-12%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">SamSung Galaxy Note 20 Ultra</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                            24.850.000&nbsp;₫     </p>
                     <p class="old-price">29.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">[Từ 1/4-30/4] giảm 30% đồng hồ samsung active 2 khi mua kèm điện thoại samsung (có điều kiện kèm theo]</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;39&nbsp;đánh giá
                                                </div>
                                 
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                        </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="15.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-9%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">SamSung Galaxy S21 Plus 5G</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                            21.700.000&nbsp;₫     </p>
                     <p class="old-price">25.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">[Từ 1/4-30/4] giảm 30% đồng hồ samsung active 2 khi mua kèm điện thoại samsung (có điều kiện kèm theo]</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;83&nbsp;đánh giá
                                            
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                        </div>
                                        </div>
         <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="37.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-24%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">SamSung Galaxy S20 Ultra</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                           18.800.000&nbsp;₫     </p>
                     <p class="old-price">29.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">[Từ 1/4-30/4] giảm 30% đồng hồ samsung active 2 khi mua kèm điện thoại samsung (có điều kiện kèm theo]</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;83&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                        </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="38.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-5%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">SamSung Galaxy Z Fold2 5G</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                            47.500.000&nbsp;₫     </p>
                     <p class="old-price">49.990.000&nbsp;₫</p>
                      </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">[Từ 1/4-30/4] giảm 30% đồng hồ samsung active 2 khi mua kèm điện thoại samsung (có điều kiện kèm theo]</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;83&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                        </div>         
             </div>
             
             <div className="container-mb-4 product-img">
            <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12 bg-white border-radius shadow-sm">
            <div className="row">
            <div className="box-title border-radius">
            <div className="col-md-3 col-sm-3 col-xs-3 box-title__title">
            <div className="row">
             <a href="#" className="text-reset text-decoration-none"><h2><strong>Laptop</strong></h2></a>
             </div>
                </div>
                <div className="col-md-9 col-sm-9 col-xs-9 box-title__tag">
            <div className="row">
            <a class="btn btn-default btn-sm" href="#">MacBook</a>
            <a class="btn btn-default btn-sm" href="#">Samsung</a>
            <a class="btn btn-default btn-sm" href="#">Dell</a>
            <a class="btn btn-default btn-sm" href="#">HP</a>
            <a class="btn btn-default btn-sm" href="#">Asus</a>
            <a class="btn btn-default btn-sm" href="#">MSI</a>       
             <a href="#">Xem tất cả</a>
            </div>
                </div>
            </div>
            <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="41.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-5%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">Apple MacBook Air M1 256GB 2020 || Chính hãng Apple Việt Nam</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                            25.000.000&nbsp;₫     </p>
                     <p class="old-price">28.990.000&nbsp;₫</p>
                      </div>                                
                    CPU:
                    8 nhân với 4 nhân hiệu năng cao và 4 nhân tiết kiệm điện
                    VGA:
                    GPU 7 nhân, 16 nhân Neural Engine
                    Ổ cứng:
                    256GB SSD
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">ƯU ĐÃI MUA MÀN HÌNH THÔNG MINH SAMSUNG KÈM LAPTOP, MACBOOK GIẢM 20% VÀ 1 KM</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;15&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="41.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-5%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                                                    <h3 className="custom-h3pro">Apple MacBook Pro 13 Touch Bar M1 16GB 256GB 2020 || Chính hãng Apple Việt Nam</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                            39.000.000&nbsp;₫     </p>
                     <p class="old-price">42.990.000&nbsp;₫</p>
                      </div>                                
                        CPU:
                        8 nhân với 4 nhân hiệu năng cao và 4 nhân tiết kiệm điện
                        VGA:
                        8 nhân GPU, 16 nhân Neural Engine
                        Ổ cứng:
                        256GB SSD
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">MUA OFFICE HOME & STUDENT 2019 KÈM MACBOOK CHỈ CÒN 1,990,000 VÀ 1 KM KHÁC</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="42.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-5%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Apple MacBook Pro 13 Touch Bar  256GB 2020 || Chính hãng Apple Việt Nam</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                            31.500.000&nbsp;₫     </p>
                     <p class="old-price">36.990.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        8 nhân với 4 nhân hiệu năng cao và 4 nhân tiết kiệm điện
                        VGA:
                        8 nhân GPU, 16 nhân Neural Engine
                        Ổ cứng:
                        256GB SSD
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">ƯU ĐÃI MUA MÀN HÌNH THÔNG MINH SAMSUNG KÈM LAPTOP, MACBOOK GIẢM 20%</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="43.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-17%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Surface Pro 7 Core i5 /8GB/128GB Chính Hãng</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                        19.900.000&nbsp;₫     </p>
                     <p class="old-price">24.990.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        8 nhân với 4 nhân hiệu năng cao và 4 nhân tiết kiệm điện
                        VGA:
                        8 nhân GPU, 16 nhân Neural Engine
                        Ổ cứng:
                        256GB SSD
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">ƯU ĐÃI MUA MÀN HÌNH THÔNG MINH SAMSUNG KÈM LAPTOP, MACBOOK GIẢM 20%</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                       
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="44.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-12%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Laptop Acer Nitro 5 AN515-55-5923</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                           19.500.000&nbsp;₫     </p>
                     <p class="old-price">24.990.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        Intel Core i5-10300H 2.5GHz up to 4.5GHz 8MB
                        VGA:
                        NVIDIA GeForce GTX 1650Ti 4GB GDDR6 + Intel UHD Graphics
                        Ổ cứng:
                        512GB SSD M.2 PCIE (Còn trống 1 khe SSD M.2 PCIE và 1 khe 2.5" SATA)

                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">ƯU ĐÃI MUA MÀN HÌNH THÔNG MINH SAMSUNG KÈM LAPTOP, MACBOOK GIẢM 20%</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                       
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="45.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-17%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Laptop Asus Zenbook 14 UX245EA</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       21.290.000&nbsp;₫     </p>
                     <p class="old-price">24.990.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        Intel® Core™ i5-1135G7 Processor, 2.40GHz up to 4.20GHz, 4 nhân 8 luồng, 8MB Intel® Smart Cache
                        VGA:
                        Intel® Iris® Xe Graphics
                        Ổ cứng:
                        512GB SSD PCIe
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">Tặng chuột không dây logitech m331 và 2 km khác</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                       
                                        </div>
                                      <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="46.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-15%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Laptop Lenovo Gaming</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                     16.990.000&nbsp;₫     </p>
                     <p class="old-price">19.990.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        Intel Core i5-9300HF 2.4GHz up to 4.1GHz 8MB
                        VGA:
                        NVIDIA GeForce GTX 1650 4GB GDDR5
                        Ổ cứng:
                        512GB SSD M.2 2242 NVMe

                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">Tặng chuột không dây </p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                       
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="47.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-17%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Laptop Asus Gaming ROG Strix G512</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       22.450.000&nbsp;₫     </p>
                     <p class="old-price">25.990.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        Intel® Core™ I5-10300H, 2.50GHz upto 4.50GHz, 8 MB cache, 4 cores 8 threads
                        VGA:
                        nVidia Geforce GTX 1650Ti 4GB DDR5
                        Ổ cứng:
                        512G M.2 NVMe™ PCIe® 3.0 SSD

                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">TẶNG CHUỘT CHƠI GAME CÓ DÂY AUSUS CERBERUS VÀ 1 KM KHÁC
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                       
                                      </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="48.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-14%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Laptop MSI Gaming GL75 Leopard</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       22.450.000&nbsp;₫     </p>
                     <p class="old-price">25.990.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        Intel® Core™ I5-10200H, 2.50GHz upto 4.50GHz, 8 MB cache, 4 cores 8 threads
                        VGA:
                        nVidia Geforce GTX 1650Ti 4GB DDR5
                        Ổ cứng:
                        512G M.2 NVMe™ PCIe® 3.0 SSD

                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">TẶNG RAM LAPTOP KINGMAX SODIMM 8GB DDR4 2666MHZ
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                       
                                        </div>
                                        
                                       {/* <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="49.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-17%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Laptop MSI</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       13.000.000&nbsp;₫     </p>
                     <p class="old-price">17.000.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        Intel Core i3-10110U 2.1GHz up to 4.1GHz 4MB
                        VGA:
                        Intel UHD Graphics
                        Ổ cứng:
                        256GB PCIe NVMe™ M.2 SSD

                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">ƯU ĐÃI MUA MÀN HÌNH THÔNG MINH SAMSUNG KÈM LAPTOP, MACBOOK GIẢM 20%
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                       
                                        </div> */}
                                        
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="50.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-17%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Laptop Lenovo Thinkbook</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       24.000.000&nbsp;₫     </p>
                     <p class="old-price">26.990.000&nbsp;₫</p>
                      </div>                                
                      CPU:
                        Intel® Core™ I7-1165G7, 2.50GHz upto 4.50GHz, 8 MB cache, 4 cores 8 threads
                        VGA:
                        nVidia Geforce GTX 1650Ti 4GB DDR5
                        Ổ cứng:
                        512G SSD

                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">TẶNG CHUỘT CHƠI GAME CÓ DÂY AUSUS CERBERUS VÀ 1 KM KHÁC
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;23&nbsp;đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                       
                                        </div>
                                        
                                        
            </div>
            </div>
            </div>
            </div>
{/* con 3div */}
<div className="container-mb-4 product-img">
            <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12 bg-white border-radius shadow-sm">
            <div className="row">
            <div className="box-title border-radius">
            <div className="col-md-3 col-sm-3 col-xs-3 box-title__title">
            <div className="row">
             <a href="#" className="text-reset text-decoration-none"><h2><strong>Tablet</strong></h2></a>
             </div>
                </div>
                <div className="col-md-9 col-sm-9 col-xs-9 box-title__tag">
            <div className="row">
            <a class="btn btn-default btn-sm" href="#">Ipad Pro</a>
            <a class="btn btn-default btn-sm" href="#">Ipad 10.2</a>
            <a class="btn btn-default btn-sm" href="#">Ipad Air</a>
            <a class="btn btn-default btn-sm" href="#">Ipad Mini</a>
            <a class="btn btn-default btn-sm" href="#">Samsung</a>       
             <a href="#">Xem tất cả</a>
            </div>  
            </div>
            </div>
            <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="51.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-7%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">SamSung Galaxy Tab S7 Plus</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       16.600.000&nbsp;₫     </p>
                     <p class="old-price">23.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
Ưu đãi mua bao da kiêm bàn phím tab S7 plus với giá 3 triệu đồng (số lượng có hạn)
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;9 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="52.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-10%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Ipad 10.2 2020 Wifi 32GB | Chính hãng Apple Việt Nam</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       8.600.000&nbsp;₫     </p>
                     <p class="old-price">11.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
Ưu đãi mua bao da kính cường lực với giá 3 triệu đồng (số lượng có hạn)
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;5 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>      
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="53.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-7%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Ipad 10.9 2020 Wifi 64GB |Chính hãng Apple Việt Nam</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       15.000.000&nbsp;₫     </p>
                     <p class="old-price">17.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
Ưu đãi mua bao da kính cường lực với giá 3 triệu đồng (số lượng có hạn)
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;9 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>    
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="54.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-7%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">SamSung Galaxy Tab S7 </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       15.800.000&nbsp;₫     </p>
                     <p class="old-price">18.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
                    Thu cũ đổi mới lên đời samsung tab - trợ giá tới 2 triệu đồng (có điều kiện kèm theo).
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;3 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="55.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-12%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Ipad Pro 11 2020 Wifi 128 GB |Chính hãng Apple Việt Nam </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       21.800.000&nbsp;₫     </p>
                     <p class="old-price">29.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
                    Tặng bao da ốp Ipad và kính cường lực với giá 3 triệu đồng
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;3 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                    
            </div>
            </div>
            </div>
            </div>
            <div className="container-mb-4 product-img">
            <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12 bg-white border-radius shadow-sm">
            <div className="row">
            <div className="box-title border-radius">
            <div className="col-md-3 col-sm-3 col-xs-3 box-title__title">
            <div className="row">
             <a href="#" className="text-reset text-decoration-none"><h2><strong>Đồng hồ thông minh</strong></h2></a>
             </div>
                </div>
                <div className="col-md-9 col-sm-9 col-xs-9 box-title__tag">
            <div className="row">
            <a class="btn btn-default btn-sm" href="#">Apple Watch</a>
            <a class="btn btn-default btn-sm" href="#">Samsung</a>
            <a class="btn btn-default btn-sm" href="#">Xiaomi</a>
            <a class="btn btn-default btn-sm" href="#">Huawie</a>
            <a class="btn btn-default btn-sm" href="#">Amazfit</a>
               
             <a href="#">Xem tất cả</a>
            </div>
            </div>
           </div>
           <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="60.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-12%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Vòng tay đeo thông minh Huawie Band 6</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       1.400.000&nbsp;₫     </p>
                     <p class="old-price">2.590.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
                    Tặng các vòng dây đeo màu khác nhau với giá 1 triệu
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;3 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="61.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-20%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Đồng hồ thông minh Xiaomi Mi Watch Lite </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                      1.800.000&nbsp;₫     </p>
                     <p class="old-price">2.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    {/* <p class="gift-cont">     
                    Tặng bao da ốp Ipad và kính cường lực với giá 3 triệu đồng
</p>       */}
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;3 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="62.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-10%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Đồng hồ Huami Amazfit GTS 2 </h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                      3.590.000&nbsp;₫     </p>
                     <p class="old-price">4.390.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    {/* <p class="gift-cont">     
                    Tặng bao da ốp Ipad và kính cường lực với giá 3 triệu đồng
</p>       */}
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;5 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="63.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-15%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Apple Watch SE 40mm Viền Nhôm Dây Cao Su | Chính Hãng Apple Việt Nam</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       7.800.000&nbsp;₫     </p>
                     <p class="old-price">9.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    {/* <p class="gift-cont">     
                    Tặng bao da ốp Ipad và kính cường lực với giá 3 triệu đồng
</p>       */}
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;7 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="66.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-8%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Apple Watch Series 6 40mm Viền Nhôm Dây Cao Su | Chính Hãng Apple Việt Nam</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       8.800.000&nbsp;₫     </p>
                     <p class="old-price">9.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
                    Tặng bao da ốp Ipad và kính cường lực với giá 3 triệu đồng
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;6 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
           </div>
           </div>
             </div>
             </div>
             <div className="container-mb-4 product-img">
            <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12 bg-white border-radius shadow-sm">
            <div className="row">
            <div className="box-title border-radius">
            <div className="col-md-3 col-sm-3 col-xs-3 box-title__title">
            <div className="row">
             <a href="#" className="text-reset text-decoration-none"><h2><strong>Âm thanh</strong></h2></a>
             </div>
                </div>
                <div className="col-md-9 col-sm-9 col-xs-9 box-title__tag">
            <div className="row">
            <a class="btn btn-default btn-sm" href="#">Loa Tivi</a>
            <a class="btn btn-default btn-sm" href="#">Loa Bluetooth</a>
            <a class="btn btn-default btn-sm" href="#">Loa vi tính</a>
            <a class="btn btn-default btn-sm" href="#">Tai nghe</a>
            <a class="btn btn-default btn-sm" href="#">Tai nghe không dây</a>
            <a class="btn btn-default btn-sm" href="#">Loa tai nghe chụp tai</a>       
             <a href="#">Xem tất cả</a>
            </div>
                </div>
            </div>
            <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="70.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-8%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Tai nghe Bluetooth Apple AirPods 2 VN/A</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       3.400.000&nbsp;₫     </p>
                     <p class="old-price">4.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
                   Giảm thêm 300.000đ khi mua kèm Apple Watch Series 6 và 1 km khác
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;10 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="71.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-13%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Tai nghe Bluetooth Apple AirPods Pro VN/A</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       5.400.000&nbsp;₫     </p>
                     <p class="old-price">7.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    <p class="gift-cont">     
                   Giảm thêm 300.000đ khi mua kèm Apple Watch Series 6 và 1 km khác
</p>      
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;12 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="72.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-8%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Tai nghe Apple EarPods Lightning |Chính hãng của Apple Viêt Nam- VN/A</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       3.400.000&nbsp;₫     </p>
                     <p class="old-price">4.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    {/* <p class="gift-cont">     
                   Giảm thêm 300.000đ khi mua kèm Apple Watch Series 6 và 1 km khác
</p>       */}
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;10 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="73.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-17%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Loa Bluetooth LG XBoom GO PL5</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                       1.600.000&nbsp;₫     </p>
                     <p class="old-price">3.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    {/* <p class="gift-cont">     
                   Giảm thêm 300.000đ khi mua kèm Apple Watch Series 6 và 1 km khác
</p>       */}
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;5 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
                                        <div className="product">
                <div className="col-md-12 col-sm-12 col-xs-12 product__box-image">
                    <a href="#">
                        <img src="74.jpg"/>
                        <div className="product__box-sticker">
                        <p className="sticker-percent">-15%</p>
                            </div>
                    </a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-info">
                    <div class="col-md-12 col-sm-12 col-xs-12 product__box-name">
                    <a href="#" class="text-reset text-decoration-none">
                             <h3 className="custom-h3pro">Loa Bluetooth LG XBoom Go PL7</h3>
                                                </a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 product__box-price">
                     <p class="special-price">
                      1.990.000&nbsp;₫     </p>
                     <p class="old-price">4.990.000&nbsp;₫</p>
                      </div>                                
                      
                  <div class="col-md-12 col-sm-12 col-xs-12 product__box-promotion">
                    {/* <p class="gift-cont">     
                   Giảm thêm 300.000đ khi mua kèm Apple Watch Series 6 và 1 km khác
</p>       */}
                        </div>
                    </div>
                   <div className="col-md-12 col-sm-12 col-xs-12 product__box-ratingresult">
                  <i class="fa fa-star checked"></i>
                           <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>
                        <i class="fa fa-star checked"></i>
                         <i class="fa fa-star checked"></i>                                                                                                                
                         &nbsp;&nbsp;12 đánh giá
                                               
                                 </div>
                                        <div className="col-md-12 col-sm-12 col-xs-12 product__box-btn">
                             <a href="#" class="btn-buy">Mua ngay</a>  &nbsp;
                             <a href="#" class="btn-compare">So sánh</a>
                                       </div>
                                        </div>
            </div>
            </div>
            </div>
            </div>
            <div className="container-mb-4 product-img">
            <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12 bg-white border-radius shadow-sm">
            <div className="row">
            <div className="box-title border-radius">
            <div className="col-md-3 col-sm-3 col-xs-3 box-title__title">
            <div className="row">
             <a href="#" className="text-reset text-decoration-none"><h2><strong>Danh mục phụ kiện</strong></h2></a>
             </div>
                </div>
                <div className="col-md-9 col-sm-9 col-xs-9 box-title__tag">
            <div className="row">
            {/* <a class="btn btn-default btn-sm" href="#">Apple Watch</a>
            <a class="btn btn-default btn-sm" href="#">Samsung</a>
            <a class="btn btn-default btn-sm" href="#">Xiaomi</a>
            <a class="btn btn-default btn-sm" href="#">Huawie</a>
            <a class="btn btn-default btn-sm" href="#">Amazfit</a> */}
               
             <a href="#">Xem tất cả</a>
            </div>
                </div>
            </div>
           </div>
           <div className="box-content cate-phukien">
               <ul>
                   <li>
                   <a href="#">
                   <div class="col-md-12 col-sm-12 col-xs-12 p-0 cate-phukien_box-icon">
                   <i class="icon-cate icon-pk-apple cpslazy loaded" data-bg="a.png" data-ll-status="loaded"></i>
                       </div>
                       <div class="col-md-12 col-sm-12 col-xs-12 cate-phukien_box-name">
                                            <div class="row">
                                                <p class="mb-0">Phụ kiện Apple</p>
                                            </div>
                                        </div>
                        </a>
                   </li>
                   <li>
                       <a href="#">
                       <div class="col-md-12 col-sm-12 col-xs-12 p-0 cate-phukien_box-icon">
                   <i class="icon-cate icon-pk-apple cpslazy loaded" data-bg="a.png" data-ll-status="loaded"></i>
                       </div>
                       <div class="col-md-12 col-sm-12 col-xs-12 cate-phukien_box-name">
                                            <div class="row">
                                                <p class="mb-0">Pin sạc sự phòng</p>
                                            </div>
                                        </div>
                       </a>
                   </li>
                   <li>
                       <a href="#">
                       <div class="col-md-12 col-sm-12 col-xs-12 p-0 cate-phukien_box-icon">
                   <i class="icon-cate icon-pk-apple cpslazy loaded" data-bg="a.png" data-ll-status="loaded"></i>
                       </div>
                       <div class="col-md-12 col-sm-12 col-xs-12 cate-phukien_box-name">
                                            <div class="row">
                                                <p class="mb-0">Dán màn hình- Dán kính cường lực</p>
                                            </div>
                                        </div>
                       </a>
                   </li>
                   <li>
                       <a href="#">
                       <div class="col-md-12 col-sm-12 col-xs-12 p-0 cate-phukien_box-icon">
                   <i class="icon-cate icon-pk-apple cpslazy loaded" data-bg="a.png" data-ll-status="loaded"></i>
                       </div>
                       <div class="col-md-12 col-sm-12 col-xs-12 cate-phukien_box-name">
                                            <div class="row">
                                                <p class="mb-0">Dây đồng hồ</p>
                                            </div>
                                        </div>
                       </a>
                   </li>
                   <li>
                       <a href="#">
                       <div class="col-md-12 col-sm-12 col-xs-12 p-0 cate-phukien_box-icon">
                   <i class="icon-cate icon-pk-apple cpslazy loaded" data-bg="a.png" data-ll-status="loaded"></i>
                       </div>
                       <div class="col-md-12 col-sm-12 col-xs-12 cate-phukien_box-name">
                                            <div class="row">
                                                <p class="mb-0">Bao da, Ốp lưng</p>
                                            </div>
                                        </div>
                       </a>
                   </li>
                   <li>
                       <a href="#">
                       <div class="col-md-12 col-sm-12 col-xs-12 p-0 cate-phukien_box-icon">
                   <i class="icon-cate icon-pk-apple cpslazy loaded" data-bg="a.png" data-ll-status="loaded"></i>
                       </div>
                       <div class="col-md-12 col-sm-12 col-xs-12 cate-phukien_box-name">
                                            <div class="row">
                                                <p class="mb-0">Cáp Sạc</p>
                                            </div>
                                        </div>
                       </a>
                   </li>
                   <li>
                       <a href="#">
                       <div class="col-md-12 col-sm-12 col-xs-12 p-0 cate-phukien_box-icon">
                   <i class="icon-cate icon-pk-apple cpslazy loaded" data-bg="a.png" data-ll-status="loaded"></i>
                       </div>
                       <div class="col-md-12 col-sm-12 col-xs-12 cate-phukien_box-name">
                                            <div class="row">
                                                <p class="mb-0">Phụ kiện Laptop</p>
                                            </div>
                                        </div>
                       </a>
                   </li>
               </ul>
           </div>
           </div>
           </div>
             </div>
             </div>
             </div>
             
            
            </div>
                   
              <div className="footer">
              <div classNameName="footer-form">
            <div classNameName="col-md-12 footer__box-contact bg-white">
                <div className="container-footer">
                <div className="row py-4">
                <div className="col-md-3 col-sm-3 col-xs-3">
                <div className="row">
                <ul className="list-unstyled m-0">
                 <li className="mb-3"> 
                <p className="font-16 mb-2"><label>Địa chỉ cửa hàng</label></p>
                <p className="mb-0"><a className="test-text" href="#">Tìm cửa hàng gần nhất</a></p>
                <p className="mb-0"><a className="test-text" href="#">Mua hàng Online</a></p>
                <div className="box-cttt">
                <p className="font-16 mb-0"><label>Phương thức thanh toán</label></p>
                <ul className="col-md-10 col-sm-10  col-sm-10 d-flex flex-wrap m-0 list-unstyled">
                {/* <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-visa"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mastercard"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-atm"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-alepay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-zalopay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-vnpay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-momo"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mpos"></em></a></li> */}
                <span class="icon"><i class="fa fa-cc-visa"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-mastercard"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-paypal"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-jcb"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-credit-card-alt"></i></span><a href="#"></a>
                </ul>
                </div>
                 </li> 
                </ul>
                </div>
              
                </div>
                <div className="col-md-3 col-sm-3 col-xs-3" >
                    <div className="row">
                    <ul className="list-unstyled m-0">
                    <p className="font-16 mb-0"><label>Dịch vụ hỗ trợ tư vấn khách hàng</label></p>
                    {/* <li className="mb-4"> */}
                    {/* <p className="mb-0">Liên hệ mua hàng: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 22h00)</p><br></br>
                    <p className="mb-0">Liên hệ bảo hành: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 21h00)</p> */}
                    <p>Liên hệ mua hàng- Hotline :1800.2020<br></br>Thời gian làm việc: 24/24</p>
                    <p>Liên hệ bảo hành- Hotline :1800.2020 <br></br>Thời gian làm việc :8h AM - 12h PM</p>
                    {/* </li> */}
                    <li>
                   </li>
                   </ul>
                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Mua hàng và thanh toán Online</p>
                        <p>Mua hàng và thanh toán trả góp qua Online</p>
                        <p>Tra cứu thông tin đơn hàng</p>
                        <p>Tra cứu thông tin bảo hành sản phẩm</p>
                        <p>Trung tâm bảo hành chính hãng</p>
                        <p>Dịch vụ bảo hành và sưa chữa điện thoại</p>

                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Quy chế hoạt động</p>
                        <p>Chính sách bảo hành</p>
                        <p>Điều kiện chính sách</p>
                        <p>Thông tin pháp lý</p>
                        <p>Giới thiệu cửa hàng</p>
                        <p>Giấy phép kinh doanh</p>

                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
              </div>
              </div>
           
        )
    }
}
export default Home;