 import React, { Component } from "react";
 import Slider from "react-slick";

 class Banner extends Component {
     
   render() {
     const settings = {
       dots: true,
       infinite: true,
       speed: 500,
       slidesToShow: 5,
       slidesToScroll: 1,
       className:"slides"
     };
     return (
       <div>
         <h2> Single Item</h2>
         <Slider {...settings}>
           <div>
             <h3> <div className="custom-img">
                                 <img src="26.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-img">
                                 <img src="27.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-img">
                                 <img src="25.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3>4 <div className="custom-img">
                                 <img src="19.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-img">
                                 <img src="21.jpg"alt=""/>
                                 </div></h3>
           </div>
           <div>
             <h3> <div className="custom-img">
                                 <img src="20.jpg"alt=""/>
                                 </div></h3>
           </div>
         </Slider>
       </div>
     );
   }
 }
 export default Banner;