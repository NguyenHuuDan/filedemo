import React, {Component, Components} from 'react';

class Pay extends Component{
    render(){
        return(
            <div>
               <div className="root">
                    <div className="header">
                    <div className="header-form">
                       <h2 className="custom-h2">MyAppStore- World Phone</h2>
                <div className="col-md-12 navbar-cps_box-main">
                                <li className="box-man"><a href="#">Trang chủ MyAppStore</a>
                             <li>  <a href=""><img src="apple.jpg"alt="" width="100px" height="100px" /> </a> </li> 
                                    <li><a href="#">Điện thoại  <span className="arrow">
                                    {/* <select multiple className="lua-chon">
                                        <option value="0">Chọn sản phẩm điện thoại bạn cần </option>
                                        <option value="34">Iphone </option>
                                        <option value="46">Samsung </option>
                                        <option value="68">Oppo </option>
                                        <option value="812">Vivo</option>
                                        </select> */}
                                        </span></a></li>
                                    <li>
                                        <input type="search" name="search" placeholder="Tìm kiếm sản phẩm..." />
                                        <span class="icon"><i class="fa fa-search"></i></span></li>
                                        {/* <ul className="form-menu">
                                        <li><a href="#" >Iphone</a></li>
                                        <li><a href="#" >Samsung</a></li>
                                        <li><a href="#" >Oppo</a></li>
                                        <li><a href="#" >Vivo</a></li>
                                        </ul> */}
                                      
                                        <li><span class="icon"><i class="fa fa-phone"></i></span><a href="#">Liên hệ</a></li> 
                                        <li><span class="icon"><i class="fa fa-shopping-cart"></i></span><a href="#">Giỏ hàng</a></li>
                                        <li><span class="icon"><i class="fa fa-cogs"></i></span><a href="#">Chăm sóc khách hàng</a> </li>
                                        <li><span class="icon"><i class="fa fa-question-circle"></i></span><a href="#">Tư vấn</a> </li>
                                        </li>
                    </div>
            </div>
                    </div>
                    </div>
                    <div className="container-main"></div>
                    <div className="footer">
                    <div classNameName="footer-form">
            <div classNameName="col-md-12 footer__box-contact bg-white">
                <div className="container-footer">
                <div className="row py-4">
                <div className="col-md-3 col-sm-3 col-xs-3">
                <div className="row">
                <ul className="list-unstyled m-0">
                 <li className="mb-3"> 
                <p className="font-16 mb-2"><label>Địa chỉ cửa hàng</label></p>
                <p className="mb-0"><a className="test-text" href="#">Tìm cửa hàng gần nhất</a></p>
                <p className="mb-0"><a className="test-text" href="#">Mua hàng Online</a></p>
                <div className="box-cttt">
                <p className="font-16 mb-0"><label>Phương thức thanh toán</label></p>
                <ul className="col-md-10 col-sm-10  col-sm-10 d-flex flex-wrap m-0 list-unstyled">
                {/* <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-visa"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mastercard"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-atm"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-alepay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-zalopay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-vnpay"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-momo"></em></a></li>
                <li className="mt-2 pl-0 pr-2"><a href="#"><em className="border rounded icon-cps-mpos"></em></a></li> */}
                <span class="icon"><i class="fa fa-cc-visa"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-mastercard"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-paypal"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-cc-jcb"></i></span><a href="#"></a>
                <span class="icon"><i class="fa fa-credit-card-alt"></i></span><a href="#"></a>
                </ul>
                </div>
                 </li> 
                </ul>
                </div>
              
                </div>
                <div className="col-md-3 col-sm-3 col-xs-3" >
                    <div className="row">
                    <ul className="list-unstyled m-0">
                    <p className="font-16 mb-0"><label>Dịch vụ hỗ trợ tư vấn khách hàng</label></p>
                    {/* <li className="mb-4"> */}
                    {/* <p className="mb-0">Liên hệ mua hàng: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 22h00)</p><br></br>
                    <p className="mb-0">Liên hệ bảo hành: <a className="text-reset font-weight-bold text-decoration-none" href="tel:18002020">1800.2020</a> (8h00 - 21h00)</p> */}
                    <p>Liên hệ mua hàng- Hotline :1800.2020<br></br>Thời gian làm việc: 24/24</p>
                    <p>Liên hệ bảo hành- Hotline :1800.2020 <br></br>Thời gian làm việc :8h AM - 12h PM</p>
                    {/* </li> */}
                    <li>
                   </li>
                   </ul>
                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Mua hàng và thanh toán Online</p>
                        <p>Mua hàng và thanh toán trả góp qua Online</p>
                        <p>Tra cứu thông tin đơn hàng</p>
                        <p>Tra cứu thông tin bảo hành sản phẩm</p>
                        <p>Trung tâm bảo hành chính hãng</p>
                        <p>Dịch vụ bảo hành và sưa chữa điện thoại</p>

                    </div>
                    </div>
                    <div className="col-md-3 col-sm-3 col-xs-3" >
                <div className="row">
                        <p>Quy chế hoạt động</p>
                        <p>Chính sách bảo hành</p>
                        <p>Điều kiện chính sách</p>
                        <p>Thông tin pháp lý</p>
                        <p>Giới thiệu cửa hàng</p>
                        <p>Giấy phép kinh doanh</p>

                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                 </div>
        )
    }
}
export default Pay;