import {Button, Form, FormGroup, Label, Input} from 'reactstrap';
import {FacebookLoginButton, GoogleLoginButton} from 'react-social-login-buttons';
import {Component} from 'react';
import {Link} from 'react-router-dom';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Email: "",
            Password: "",
            Address: "",
        };
        this.onHandleChange = this.onHandleChange.bind(this);
        this.onHandleSubmit = this.onHandleSubmit.bind(this);
         
    }


    onHandleChange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value,
        });
    }

    onHandleSubmit(event) {
        alert("Welcom to MyAppStore  " + JSON.stringify(this.state.Email)+ JSON.stringify(this.state.Password)+ JSON.stringify(this.state.Address));
        event.preventDefault();
        console.log(this.state);
    }

    render() {
        return (
            <div onSubmit={this.onHandleSubmit}>
                <Form className="login-form">
                    <h1 className="custom-h1">Welcom to MyAppStore</h1>
                    <h2 className="text-center">Login MyAppStore</h2>
                    <FormGroup>
                        <Label>Email</Label>
                        <Input type="Email" name="Email" placeholder="Email" onChange={this.onHandleChange}
                               value={this.state.Email} />
                    </FormGroup>
                    <FormGroup>
                        <Label>Mật khẩu</Label>
                        <Input type="Password" name="Password" placeholder="Password" onChange={this.onHandleChange}
                               value={this.state.Password}/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Địa chỉ</Label>
                        <Input type="Address" name="Address" placeholder="Address" onChange={this.onHandleChange}
                               value={this.state.Addres}/>
                    </FormGroup>
                    <Button className="btn-lg btn-dark btn-block">Đăng Nhập</Button>
                    <div className="text-center pt-3">
                        Hoặc đăng nhập với
                    </div>
                    <FacebookLoginButton className="mt-3 mb-3">Đăng nhập bằng Facebook</FacebookLoginButton>
                    <GoogleLoginButton className="mt-3 mb-3"> Đăng nhập bằng Google</GoogleLoginButton>
                    <div className="text-center">

                        {/* <a href="/signup">Đăng Kí</a> */}
                        {/* <button onClick={this.onhandleClick}>
                             Đăng Kí
                        </button> */}
                        <Link to="/signup">Đăng Kí</Link>

                        <span className="p-2">||</span>
                        <Link to="/forgot">Quên mật khẩu</Link>
                    </div>
                    <div className="container">
                        <div className="row">

                            <div className="col-sm-4">

                                <div className="custom_target">
                                    <a target="_blank"
                                       href="https://www.apple.com/legal/sales-support/applecare/docs/techsupportvi.html">Trợ
                                        giúp</a>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="custom_target">
                                    <a target="_blank" href="https://www.apple.com/legal/privacy/vn/">Chính sách bảo
                                        mật</a>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="custom_target">
                                    <a target="_blank"
                                       href="https://www.apple.com/legal/internet-services/itunes/vn/terms.html">Điều
                                        khoản sử dụng</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>
            </div>
        );
    }
}

export default Login;
