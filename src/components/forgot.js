import React, { Component } from 'react';
import {Button, Form, FormGroup, Label, Input} from 'reactstrap';
class Forgot extends Component{
    constructor(props) {
        super(props);
        this.state = {
            Password: "",
            Password1: "",
            Confirm: "",
        };
        this.onHandleChange = this.onHandleChange.bind(this);
        this.onHandleSubmit = this.onHandleSubmit.bind(this);
         
    }


    onHandleChange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value,
        });
    }
    onHandleSubmit(event) {
        alert("Mật khẩu mới là:  " + JSON.stringify(this.state.Confirm));
        event.preventDefault();
        console.log(this.state);
    }
    render(){
        return(
            <div>
                      <div onSubmit={this.onHandleSubmit}>
                <Form className="login-form">
                <h1 className="custom-h1">Welcom to MyAppStore</h1>
                    <h2 className="text-center">Forgot Password</h2>
                    <FormGroup>
                    <Label>Mật khẩu cũ</Label>
                    <Input type="Password" name="Password" placeholder="Nhập một mật khẩu bất kỳ!" onChange={this.onHandleChange}
                           value={this.state.Password}/>
                           <Label>Mật khẩu mới</Label>
                    <Input type="Password1" name="Password1" placeholder="Nhập mật khẩu mới" onChange={this.onHandleChange}
                           value={this.state.Password1}/>
                </FormGroup>
                <FormGroup>
                    <Label>Xác nhận mật khẩu mới</Label>
                    <Input type="Confirm" name="Confirm" placeholder="Xác nhận mật khẩu mới" onChange={this.onHandleChange}
                           value={this.state.Confirm}/>
                </FormGroup>
                <Button className="btn-lg btn-dark btn-block">Xác nhận</Button>
                </Form>
                </div>
               
            </div>
        )
    }
}
export default Forgot;